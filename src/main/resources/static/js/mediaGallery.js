var videos = $('video'),
    videoMethods = {
        init: function () {
            this.setHeightEqualsToWidth();
            this.onClick();
            this.onEnded();
        },

        setHeightEqualsToWidth: function () {
            videos.each(function (i) {
                //this.style.height = this.clientWidth + 'px';
            });
        },

        onClick: function () {
            videos.click(function () {
                if (this.requestFullscreen) {
                    this.requestFullscreen();
                } else if (this.msRequestFullscreen) {
                    this.msRequestFullscreen();
                } else if (this.mozRequestFullScreen) {
                    this.mozRequestFullScreen();
                } else if (this.webkitRequestFullScreen) {
                    this.webkitRequestFullScreen();
                }
                this.classList.remove('has-media-controls-hidden');
                this.setAttribute('controls', 'controls');
                this.play();
            });
        },

        onEnded: function () {
            videos.on("ended", function () {
                this.classList.add('has-media-controls-hidden');
                this.removeAttribute("controls");
                var ownerDoc = this.ownerDocument;
                if (ownerDoc.exitFullscreen) {
                    ownerDoc.exitFullscreen();
                } else if (ownerDoc.msExitFullscreen) {
                    ownerDoc.msExitFullscreen();
                } else if (ownerDoc.mozCancelFullScreen) {
                    ownerDoc.mozCancelFullScreen();
                } else if (ownerDoc.webkitExitFullscreen) {
                    ownerDoc.webkitExitFullscreen();
                }
            });
        }
    };

videoMethods.init();