// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require_tree .
//= require_self
var TBT = {};

/**
 * ACADEMY MENU
 */
TBT.MainMenu = (function () {
    return {
        init: function () {
            $('.main-menu-items-button').click(function () {
                $('.main-menu-items').toggle()
            });

            $('.main-menu-items .close-menu').click(function () {
                $('.main-menu-items').toggle()
            });

            $('.main-menu-items .add-video').click(function () {
                $('.add-video-items').toggle()
            });
        }
    }

})();


/**
 * ACADEMY MENU
 */
TBT.AcademyMenu = (function () {
    var previous = null;

    var cleaUp = function () {
        $('.skills').hide();
        $('.category').removeClass('show-menu');
    };

    var toggle = function (id) {
        var current = '#' + id + '_skills';

        if (current !== previous) {
            $(current).show();
            $('#' + id).addClass('show-menu');
        } else {
            current = null;
        }

        previous = current;
    };

    return {
        init: function () {
            $('.category').click(function () {
                cleaUp();
                toggle(this.id);
            });

            $('.skills .close-menu').click(function () {
                previous = null;
                cleaUp();
            });
        }
    }

})();


/**
 * ACADEMY MENU
 */
TBT.AcademyShowMore = (function () {
    return {
        init: function () {
            $('.show-more').click(function () {
                $('.complementary-lessons').show();
                this.remove();
            });
        }
    }
})();


/**
 * FORM SUBMITION BUTTON
 */
TBT.FormSubmitButton = (function () {
    var fieldName = 'data';

    var isJSON = function ($form) {
        return $form.attr('meta') === 'json';
    };

    var toJSON = function ($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function (n, i) {
            if (n['name'] !== fieldName) {
                indexed_array[n['name']] = n['value'];
            }
        });

        return JSON.stringify(indexed_array);
    };

    var nativeSubmit = function ($form) {
        $form.submit();
    };

    var getDataElement = function () {
        var $element = $('#form-json-data');
        return $element.length ? $element :
            $(document.createElement('input'))
                .attr('id', 'form-json-data')
                .attr('type', 'hidden')
                .attr('name', fieldName);
    };

    var jsonNativeSubmit = function ($form) {
        var $input = getDataElement().attr('value', toJSON($form));
        $form.append($input);
        nativeSubmit($form);
    };

    var getSubmit = function ($form) {
        return isJSON($form) ? jsonNativeSubmit : nativeSubmit;
    };

    return {
        init: function () {
            $('.submit').click(function () {
                var $form = $(this).closest('form');
                getSubmit($form)($form);
            });
        }
    }
})();


/**
 * Translations
 */

TBT.I18n = (function () {
    return {
        get: function (message, def) {
            return window.I18N[message] || window.I18N[def];
        }
    }
})();

/**
 * Dialogs
 */
TBT.Dialog = (function () {
    var produceRaw = function (level, message) {
        return $([
            '<div class="row" style="display: none">',
            '<div class="col-md-12 dialog-fixed">',
            '<div class="dialog ' + level + '">',
            '<div class="shape"><div class="glyphicon glyphicon-remove-circle"></div></div>',
            message,
            '</div>',
            '</div>',
            '</div>'
        ].join("\n"));
    };

    var clean = function () {
        $('.dialog').fadeOut();
    };

    var show = function (dialog) {
        $('#dialog-host').append(dialog);
        dialog.fadeIn(100);
        window.setTimeout(function () {
            dialog.fadeOut('slow', function () {
                clean();
            });
        }, 3000);
    };

    return {
        error: function (message) {
            clean();
            show(produceRaw('error', message));
        },
        success: function (message) {
            clean();
            show(produceRaw('success', message));
        }
    }
})();

/**
 * Loader overlay
 */
TBT.Overlay = (function () {
    return {
        show: function (duration, complete) {
            $('#overlay').fadeIn(duration, complete);
        },
        hide: function (duration, complete) {
            $('#overlay').fadeOut(duration, complete);
        }
    }
})();


/**
 * Academy challenge file upload
 */
(function () {
    var $button = $('.button.upload');
    var $progres = $('#overlay .progress-bar');

    var setProgressBar = function (evt) {
        if (evt.lengthComputable) {
            var width = (evt.loaded / evt.total) * 100 + '%';
            $progres.width(width);
        }
    };

    var initVideo = function (courseId, type, size, success) {
        $.ajax({
            url: '/api/academy/video',
            type: "POST",
            data: {
                courseId: courseId,
                type: type,
                size: size
            },
            success: success,
            error: function (resp, body) {
                TBT.Overlay.hide(100);
                var err = eval("(" + resp.responseText + ")");
                TBT.Dialog.error(TBT.I18n.get(err.error, 'tbt.form.upload.error'));
            }
        })
    };

    var createCORSRequest = function (method, url) {
        var xhr = new XMLHttpRequest();
        if ('withCredentials' in xhr) {
            xhr.open(method, url, true)
        } else if (typeof XDomainRequest !== 'undefined') {
            xhr = new XDomainRequest();
            xhr.open(method, url)
        } else {
            xhr = null
        }
        return xhr
    };


    var uploadVideo = function (url, file, success) {
        var xhr = createCORSRequest('PUT', url);
        xhr.onload = function () {
            if (xhr.status === 200) {
                success();
            } else {
                TBT.Overlay.hide(100);
                TBT.Dialog.error(TBT.I18n.get(resp.responseText, 'tbt.form.upload.error'));
            }
        };
        xhr.onerror = function () {
            TBT.Overlay.hide(100);
            TBT.Dialog.error(TBT.I18n.get(resp.responseText, 'tbt.form.upload.error'));
        };
        xhr.upload.addEventListener("progress", setProgressBar, false);
        xhr.addEventListener("progress", setProgressBar, false);
        xhr.setRequestHeader('Content-Type', file.type);
        xhr.send(file);
    };

    var confirmVideo = function (id, success) {
        $.ajax({
            url: '/api/academy/video/' + id,
            type: "PUT",
            success: success,
            error: function (resp) {
                TBT.Overlay.hide(100);
                TBT.Dialog.error(TBT.I18n.get(resp.responseText, 'tbt.form.upload.error'));
            }
        })
    };

    var $file = $('#file').change(function () {
        var courseId = $button.attr('course');
        var formData = new FormData();
        formData.append('file', this.files[0], this.files[0].name);
        var type = this.files[0].type;
        var size = this.files[0].size;
        var file = this.files[0];

        TBT.Overlay.show(100, function () {
            initVideo(courseId, type, size, function (resp) {
                uploadVideo(resp.url, file, function () {
                    confirmVideo(resp.id, function () {
                        TBT.Overlay.hide(100);
                        TBT.Dialog.success(TBT.I18n.get(resp.responseText, 'tbt.academy.challenge.success'));
                    });
                });
            });
        });
    });

    $button.click(function () {
        if ($(this).hasClass('loader')) {
            return;
        }

        $file.click();
    });

})();

/**
 * YT videos
 */
(function () {
    var youtube = document.querySelectorAll(".youtube");

    var track = function (lessonId, skillId, courseId, state) {
        $.ajax({
            url: '/api/activity/academy/video',
            type: "POST",
            data: {
                state: state,
                skillId: skillId,
                courseId: courseId,
                lessonId: lessonId
            },
            enctype: 'application/json'
        });
    };

    for (var i = 0; i < youtube.length; i++) {
        var source = "https://img.youtube.com/vi/" + youtube[i].id + "/sddefault.jpg";

        var image = new Image();
        image.src = source;
        image.addEventListener("load", function () {
            youtube[i].appendChild(image);
        }(i));


        youtube[i].addEventListener("click", function () {
            var id = 'player_' + this.id;
            var $this = $(this);
            var lessonId = $(this).attr('lesson');
            var skillId = $(this).attr('skill');
            var courseId = $(this).attr('course');

            $this.empty().append($('<div id="' + id + '"></div>'));

            new YT.Player(id, {
                videoId: this.id,
                events: {
                    'onStateChange': function (event) {
                        if (!lessonId) {
                            return;
                        }

                        switch (event.data) {
                            case YT.PlayerState.PLAYING:
                                track(lessonId, skillId, courseId, 'started');
                                break;
                            case YT.PlayerState.PAUSED:
                                track(lessonId, skillId, courseId, 'paused');
                                break;
                            case YT.PlayerState.ENDED:
                                track(lessonId, skillId, courseId, 'seen');
                                break;
                        }
                    }
                }
            });
        });
    }
})();

TBT.Star = (function () {
    var getShape = function (size, percentage) {
        return $([
            '<svg id="ratingstar" width="' + size + '" height="' + size + '" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">',
            '<linearGradient id="lg" x1="0.5" y1="1" x2="0.5" y2="0">',
            '<stop offset="0%" stop-opacity="1" stop-color="#E2CB5B"/>',
            '<stop offset="' + percentage + '%" stop-opacity="1" stop-color="#E2CB5B"/>',
            '<stop offset="' + (5 + percentage) + '%" stop-color="#000000"/>',
            '<stop offset="100%" stop-opacity="1" stop-color="#000000"/>',
            '</linearGradient>',
            '<path stroke="#000000" fill="url(#lg)" style="background-color:white" id="svg_1" d="m3,39.118059l36.668998,0l11.331002,-36.668838l11.331009,36.668838l36.668991,0l-29.665788,22.662329l11.331589,36.668838l-29.665801,-22.662946l-29.665794,22.662946l11.331592,-36.668838l-29.665798,-22.662329z" fill-opacity="null" stroke-opacity="null" stroke-width="0"/>',
            '</svg>'
        ].join("\n"));
    };

    return {
        draw: function (target, size, percent) {
            var _target = $(target);
            var _percent = percent || _target.attr('percent');
            _target.append(getShape(size, _percent));
        },
        init: function () {
            $('.draw-star').each(function () {
                var _target = $(this);
                var _percent = parseInt(_target.attr('percent'));
                var _size = _target.attr('size');
                _target.append(getShape(_size, _percent));
            });
        }
    }
})();

/**
 * Floating widget
 */
TBT.ScoreDialog = (function () {

    var _element = null;

    var _removeLoader = function () {
        _element.find('.content').removeClass('loader')
    };

    var _setMaxValue = function (current, max) {
        if (current == max) {
            _element.find('.total').remove();
        } else {
            _element.find('.total').text(max);
        }
    };

    var _currentValue = function (current, max) {
        var percentage = 10 * current;
        _element.find('.bar').width(percentage + '%');
        _element.find('.current').text(current);
        TBT.Star.draw(_element.find('.star'), 43, percentage);
    };

    var _updateScore = function () {
        $.ajax({
            url: '/score',
            type: "GET",
            cache: false,
            success: function (resp) {
                _setMaxValue(resp.current, resp.max);
                _currentValue(resp.current, resp.max);
                _removeLoader();
            },
            error: function (resp) {
            }
        });
    };

    return {
        init: function () {
            _element = $('#tbt-score-widget');
            _element.length && _updateScore();
        }
    }
})();


// INIT ON PAGE
TBT.MainMenu.init();
TBT.AcademyMenu.init();
TBT.FormSubmitButton.init();
TBT.AcademyShowMore.init();
TBT.ScoreDialog.init();
TBT.Star.init();


