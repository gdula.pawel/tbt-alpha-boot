var TBT = {};

/**
 * PROFILE PAGE SLIDERS
 */
TBT.ProfileSliders = $(function () {

    $('input.userProfileSlider').each(function (i) {
        var id = this.id;
        var sliderValueElement = document.getElementById(id.replace('userSlider', 'userSliderValue'));
        var slider = new Slider('#' + id, {value: sliderValueElement.textContent});
        slider.on("change", function (slider) {
            sliderValueElement.textContent = slider.newValue;
            $('#' + id).closest('form').find('button').show();
        });
    });

    $('input.tbtProfileSlider').each(function (i) {
        var id = this.id;
        var sliderValueElement = document.getElementById(id.replace('tbtSlider', 'tbtSliderValue'));
        var slider = new Slider('#' + id, {value: sliderValueElement.textContent});
        slider.on("change", function (slider) {
            sliderValueElement.textContent = slider.newValue;
        });
    });

    $(function () {
        $("#profileTabs").tabs();
    });

});

TBT.UserData = $(function () {

    $('#dateOfBirth').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:' + (new Date()).getFullYear(),
        onSelect: function (dateText, elem) {
            $(this).closest('form').find('button').show();
        }
    });
    $('.profileBox select').each(function (index, elem) {
        $(elem).selectmenu({
            change: function (event, ui) {
                $(this).closest('form').find('button').show();
            }
        });
    });

    $('.profileBox input').each(function (index, elem) {
        $(elem).keydown(function () {
            $(this).closest('form').find('button').show();
        });
    });

});

// INIT ON PAGE
TBT.ProfileSliders.init();
TBT.UserData.init();
