var TBT = TBT || {};

/**
 * FORM SUBMITION BUTTON
 */
TBT.FormSubmitButton = (function () {
    var fieldName = 'data';

    var isJSON = function ($form) {
        return $form.attr('meta') === 'json';
    };

    var toJSON = function ($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function (n, i) {
            if (n['name'] !== fieldName) {
                indexed_array[n['name']] = n['value'];
            }
        });

        return JSON.stringify(indexed_array);
    };

    var nativeSubmit = function ($form) {
        $form.submit();
    };

    var getDataElement = function () {
        var $element = $('#form-json-data');
        return $element.length ? $element :
            $(document.createElement('input'))
                .attr('id', 'form-json-data')
                .attr('type', 'hidden')
                .attr('name', fieldName);
    };

    var jsonNativeSubmit = function ($form) {
        var $input = getDataElement().attr('value', toJSON($form));
        $form.append($input);
        nativeSubmit($form);
    };

    var getSubmit = function ($form) {
        return isJSON($form) ? jsonNativeSubmit : nativeSubmit;
    };

    return {
        init: function () {
            $('.submit').click(function () {
                var formId = $(this).attr('for');
                var $form = $('#' + formId);
                getSubmit($form)($form);
            });
        }
    }
})();

TBT.FormSubmitButton.init();

/**
 * SERIALIZATION FOR MULTISELECT
 */

TBT.MultiSelectSerializer = (function () {
    return {
        init: function () {
            $('.serializable').change(function () {
                $select = $(this);
                $hidden = $('#' + $select.attr('for'));
                var value = $select.val() ? $select.val().join(',') : '';
                $hidden.val(value);
                console.log('action')
            });
        }
    }
})();

TBT.MultiSelectSerializer.init();