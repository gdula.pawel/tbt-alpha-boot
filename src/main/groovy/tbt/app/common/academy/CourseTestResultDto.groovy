package tbt.app.common.academy

import tbt.app.domains.academy.Course

class CourseTestResultDto {

    final List<Long> correctAnswers = []

    final List<Long> submittedQuestions

    final int numberOfQuestions

    CourseTestResultDto(Course course, List<Long> submittedQuestions) {
        this.numberOfQuestions = course.quiz.questions.size()
        this.submittedQuestions = submittedQuestions
    }

    boolean isAllQuestionsSubmitted() {
        return submittedQuestions.size() == numberOfQuestions
    }

    float score() {
        return ((correctAnswers.size() / numberOfQuestions) * 100)
    }
}

