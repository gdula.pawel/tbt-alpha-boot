package tbt.app.common.academy

import org.apache.tomcat.util.http.fileupload.FileItemStream

class UploadedFileDto {

    final FileItemStream file

    UploadedFileDto(FileItemStream file) {
        this.file = file
    }

    String getContentType() {
        file.getContentType()
    }

    String getSuffix() {
        file.getContentType().tokenize('/').last()
    }

}
