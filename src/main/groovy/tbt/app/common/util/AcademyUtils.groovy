package tbt.app.common.util

import tbt.app.common.skill.SkillDto
import tbt.app.domains.academy.Answer
import tbt.app.domains.academy.Lesson
import tbt.app.domains.academy.Question

class AcademyUtils {

    static Question getQuestion(final List<Question> questions, final Integer idx) {
        questions?.find { it.idx == idx }
    }

    static Answer getAnswer(final List<Question> questions, final Integer qIdx, final Integer aIdx) {
        getQuestion(questions, qIdx)?.answers?.find { it.idx == aIdx }
    }

    static Lesson getLesson(final List<Lesson> lessons, final Integer idx) {
        lessons?.find { it.idx == idx }
    }

    static String getLessonTagsIdsAsStrings(final List<Lesson> lessons, final Integer idx) {
        getLesson(lessons, idx)?.tags?.collect { it.id }?.join(',')
    }

    static String hasTests(final List<SkillDto> skills) {
        skills.find { it.hasAcademyTest }
    }

}
