package tbt.app.common.util

import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest

@Slf4j
class ReferralUtils {

    private static final String URL_USER_ID_PARAM = '_uid'

    static isSet(HttpServletRequest request) {
        request.getParameter(URL_USER_ID_PARAM) != null
    }

    static Long store(HttpServletRequest request) {
        SessionUtils.put(SessionUtils.REFERRING_USER_ID, getUID(request))

        try {
            request.getParameter(URL_USER_ID_PARAM)?.toLong()
        } catch (Exception exp) {
            log.error("There was an error while converting user id", exp)
        }
    }

    private static Long getUID(HttpServletRequest request) {
        try {
            return request.getParameter('_uid')?.toLong()
        } catch (Exception exp) {
            log.error("There was an error while converting user id", exp)
            return null
        }
    }

    static Long get() {
        SessionUtils.get(SessionUtils.REFERRING_USER_ID, Long)
    }


}
