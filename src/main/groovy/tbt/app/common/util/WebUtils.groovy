package tbt.app.common.util;

class WebUtils {

    static String redirectTo(final String target) {
        return "redirect:" + target
    }

    static String redirectTo(final String target, final Object... elements) {
        redirectTo(elements.inject(target) { r, i -> r.replaceFirst('\\{\\}', i.toString()) })
    }

}
