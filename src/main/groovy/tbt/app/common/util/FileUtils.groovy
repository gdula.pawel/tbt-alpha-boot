package tbt.app.common.util

import org.springframework.util.MimeType

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.security.MessageDigest

class FileUtils {

    static InputStream download(final String url, final String formatName) {
        BufferedImage img = ImageIO.read(new URL(url))
        ByteArrayOutputStream os = new ByteArrayOutputStream()
        ImageIO.write(img, formatName, os)
        return new ByteArrayInputStream(os.toByteArray())
    }

    static String generateName(final Object internalIdentifier, final String type) {
        "${md5(internalIdentifier)}-${UUID.randomUUID()}.${type}"
    }

    static String generateName(final Object internalIdentifier, final MimeType mimeType) {
        generateName(internalIdentifier, mimeType.subtype)
    }

    private static md5(final Object input) {
        MessageDigest.getInstance("MD5").digest(input.toString().bytes).encodeHex().toString()
    }
}
