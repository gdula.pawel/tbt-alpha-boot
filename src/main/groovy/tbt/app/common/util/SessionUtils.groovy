package tbt.app.common.util

import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpSession

class SessionUtils {

    static final String REFERRING_USER_ID = 'referringUserId'


    static put(final String key, final Object value) {
        session.setAttribute(key, value)
    }

    static <T> T get(final String key, Class<T> type) {
        (T) session.getAttribute(key)
    }

    private static HttpSession getSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true)
    }

}
