package tbt.app.common.util

import java.text.SimpleDateFormat

/**
 * Created by KKACHEL on 23.12.2017.
 */
final class AppConstantsUtils {

    public static final String DATE_PATTERN = "dd/MM/yyyy"

    public static final DATE_FORMAT = new SimpleDateFormat(DATE_PATTERN)

    //Hide the constructor
    private TbtConstants() {}

}
