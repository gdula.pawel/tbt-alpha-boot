package tbt.app.common.util

import tbt.app.domains.academy.Lesson
import tbt.app.domains.academy.Question
import tbt.app.domains.i18n.Text

class I18nUtils {

    static String getQuestion(final List<Question> questions, final Integer idx, final Locale locale) {
        get(AcademyUtils.getQuestion(questions, idx)?.text, locale)
    }

    static String getAnswer(
            final List<Question> questions, final Integer qIdx, final Integer aIdx, final Locale locale) {
        get(AcademyUtils.getAnswer(questions, qIdx, aIdx)?.text, locale)
    }

    static String getLesson(final List<Lesson> lessons, final Integer idx, final Locale locale) {
        get(AcademyUtils.getLesson(lessons, idx)?.text, locale)
    }

    static String get(final List<Text> text) {
        text?.find { it.locale == locale }?.content
    }

    static String get(final List<Text> text, final Locale locale) {
        text?.find { it.locale == locale }?.content
    }
    
    /**
     * Used as fallback method in case when we use direct text value
     * @see /cms/acacdemy Test error handling
     */
    static String get(final String text, final Locale locale) {
        text
    }

    private static getLocale() {
        Locale.FRANCE
    }

}
