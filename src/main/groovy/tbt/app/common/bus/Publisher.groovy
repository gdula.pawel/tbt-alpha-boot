package tbt.app.common.bus

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import reactor.bus.Event
import reactor.bus.EventBus
import tbt.app.domains.activity.Activity

@Slf4j
@Component
class Publisher {

    @Autowired
    EventBus eventBus

    void publish(final Activity activity) throws InterruptedException {
        try {
            eventBus.notify("activity", Event.wrap(activity))
        } catch (exp) {
            log.warn('Exception when publishing an even', exp)
        }

    }

}
