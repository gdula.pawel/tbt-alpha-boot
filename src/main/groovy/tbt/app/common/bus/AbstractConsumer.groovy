package tbt.app.common.bus

import reactor.bus.EventBus
import reactor.fn.Consumer

import javax.annotation.PostConstruct

import static reactor.bus.selector.Selectors.$

abstract class AbstractConsumer<T> implements Consumer<T> {

    private final EventBus eventBus

    AbstractConsumer(EventBus eventBus) {
        this.eventBus = eventBus
    }

    @PostConstruct
    init() {
        eventBus.on($(key), this)
    }

    protected abstract String getKey()


}
