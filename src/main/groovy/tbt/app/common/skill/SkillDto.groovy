package tbt.app.common.skill

import tbt.app.domains.skill.Category
import tbt.app.domains.skill.Skill

class SkillDto {

    final public Long id

    final public String code

    final public Category category

    final public String categoryGroup

    final boolean hasAcademyTest

    SkillDto(final Skill skill, final boolean hasAcademyTest) {
        this.id = skill.id
        this.code = skill.code
        this.category = skill.category
        this.categoryGroup = skill.categoryGroup
        this.hasAcademyTest = hasAcademyTest
    }
}
