package tbt.app.common.file

enum FileManagerType {
    LOCAL,
    GOOGLE_STORAGE
}