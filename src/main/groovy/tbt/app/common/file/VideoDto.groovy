package tbt.app.common.file

class VideoDto {

    final Long id

    final String name

    final String poster

    final String path

    VideoDto(Long id, String name, String path, String poster) {
        this.id = id
        this.name = name
        this.poster = poster
        this.path = path
    }

    VideoDto(String path) {
        this.path = path
        this.name = null
        this.poster = null
    }

    String getMimeType() {
        return "video/mp4"
    }

    String getType() {
        path.tokenize('.').last().toLowerCase().trim()
    }
}
