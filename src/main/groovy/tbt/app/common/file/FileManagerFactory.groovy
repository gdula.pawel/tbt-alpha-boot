package tbt.app.common.file

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class FileManagerFactory {

    @Value('${tbt.fileManager.type}')
    private FileManagerType fileManagerType

    FileManager get() {
        switch (fileManagerType) {
            case FileManagerType.GOOGLE_STORAGE:
                return new GoogleStorageFileManager()
            default:
                throw new IllegalStateException("Not supported file manager ${fileManagerType}")
        }
    }

}
