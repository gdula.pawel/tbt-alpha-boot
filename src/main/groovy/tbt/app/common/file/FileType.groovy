package tbt.app.common.file

enum FileType {
    VIDEO,
    VIDEO_POSTER,
    AVATAR
}