package tbt.app.common.file

import org.springframework.util.MimeType


interface FileManager {

    void upload(String fileName, FileType fileType, String contentType, InputStream file)

    String getPath(String fileName, FileType fileType)

    URL getUploadPath(String fileName, MimeType mimeType, FileType fileType)

}