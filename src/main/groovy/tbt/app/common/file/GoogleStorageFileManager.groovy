package tbt.app.common.file

import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.WriteChannel
import com.google.cloud.storage.*
import groovy.util.logging.Slf4j
import org.springframework.util.MimeType

import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

@Slf4j
class GoogleStorageFileManager implements FileManager {

    private static final String STORAGE_CREDENTIALS_FILE = 'TheBestTeam-8dfec8212321.json'

    @Override
    void upload(final String fileName, final FileType fileType, final String contentType, final InputStream input) {
        // based on https://goo.gl/r5ymB1
        final BlobId blobId = BlobId.of(getBucket(fileType), fileName)
        final BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(contentType).build()
        WriteChannel writer = storage.writer(blobInfo, Storage.BlobWriteOption.doesNotExist())
        try {
            byte[] buffer = new byte[1024]
            int limit
            while ((limit = input.read(buffer)) >= 0) {
                try {
                    writer.write(ByteBuffer.wrap(buffer, 0, limit))
                } catch (Exception ex) {
                    log.error('Error while uploading bytes', ex)
                }
            }
        } finally {
            writer.close()
        }
    }

    @Override
    String getPath(final String fileName, final FileType fileType) {
        fileName ? "https://${getBucket(fileType)}.storage.googleapis.com/${fileName}" : null
    }

    @Override
    URL getUploadPath(final String fileName, final MimeType mimeType, final FileType fileType) {
        storage.signUrl(
                BlobInfo.newBuilder(getBucket(fileType), fileName).setContentType(mimeType.toString()).build(),
                1,
                TimeUnit.HOURS,
                Storage.SignUrlOption.httpMethod(HttpMethod.PUT),
                Storage.SignUrlOption.withContentType()
        )
    }

    private Storage getStorage() {
        StorageOptions.newBuilder().setCredentials(credentials).build().getService()
    }

    private ServiceAccountCredentials getCredentials() {
        ServiceAccountCredentials.fromStream(credentialsFileStream)
    }

    private InputStream getCredentialsFileStream() {
        Thread.currentThread().contextClassLoader.getResourceAsStream(STORAGE_CREDENTIALS_FILE)
    }

    private String getBucket(final FileType fileType) {
        "tbt-${fileType.name().toLowerCase().replaceAll('_', '-')}"
    }
}
