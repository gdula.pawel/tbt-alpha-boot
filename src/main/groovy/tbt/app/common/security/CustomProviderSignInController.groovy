package tbt.app.common.security

import org.springframework.social.connect.ConnectionFactoryLocator
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.connect.web.ProviderSignInController
import org.springframework.social.connect.web.SignInAdapter
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.servlet.view.RedirectView

class CustomProviderSignInController extends ProviderSignInController {

    CustomProviderSignInController(ConnectionFactoryLocator connectionFactoryLocator,
                                   UsersConnectionRepository usersConnectionRepository,
                                   SignInAdapter signInAdapter) {
        super(connectionFactoryLocator, usersConnectionRepository, signInAdapter)
    }

    @RequestMapping(value = "/{providerId}", method = RequestMethod.GET, params = "error")
    RedirectView oauth2ErrorCallback(@PathVariable String providerId,
                                     @RequestParam("error") String error,
                                     @RequestParam(value = "error_description", required = false) String errorDescription,
                                     @RequestParam(value = "error_uri", required = false) String errorUri,
                                     NativeWebRequest request) {
        return new RedirectView('/?status=' + error, true)
    }
}
