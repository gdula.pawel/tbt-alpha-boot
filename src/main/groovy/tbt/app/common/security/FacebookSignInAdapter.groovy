package tbt.app.common.security

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.social.connect.Connection
import org.springframework.social.connect.web.SignInAdapter
import org.springframework.web.context.request.NativeWebRequest
import tbt.app.domains.user.Role

class FacebookSignInAdapter implements SignInAdapter {

    private final String redirectTo

    FacebookSignInAdapter(String redirectTo) {
        this.redirectTo = redirectTo
    }

    @Override
    String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        localUserId,
                        null,
                        [new SimpleGrantedAuthority(Role.USER.asAuthority())]))

        return redirectTo
    }
}
