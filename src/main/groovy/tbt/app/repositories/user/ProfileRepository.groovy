package tbt.app.repositories.user;

import org.springframework.data.repository.CrudRepository;
import tbt.app.domains.user.Profile;

public interface ProfileRepository extends CrudRepository<Profile, Long> {
}
