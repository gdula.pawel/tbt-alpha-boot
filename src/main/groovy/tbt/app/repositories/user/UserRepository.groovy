package tbt.app.repositories.user

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.user.User

interface UserRepository extends CrudRepository<User, Long> {

    User findByExternalId(String externalId)
}
