package tbt.app.repositories.i18n

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.i18n.Text

interface TextRepository extends CrudRepository<Text, Long> {
}
