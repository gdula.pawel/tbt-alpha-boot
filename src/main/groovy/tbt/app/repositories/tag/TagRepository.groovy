package tbt.app.repositories.tag

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.tag.Tag

interface TagRepository extends CrudRepository<Tag, Long> {

    Tag findByNameAndLocale(String name, Locale locale)

    List<Tag> findAllByOrderByName()
}
