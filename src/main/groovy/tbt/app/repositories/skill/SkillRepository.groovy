package tbt.app.repositories.skill

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import tbt.app.domains.skill.Skill

interface SkillRepository extends CrudRepository<Skill, Long> {

    Skill findByCode(String code)

    @Query("select s from Skill s order by s.category, s.categoryGroup, s.code")
    List<Skill> findAll()

}
