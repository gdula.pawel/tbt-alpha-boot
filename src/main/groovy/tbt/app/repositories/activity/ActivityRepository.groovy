package tbt.app.repositories.activity

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType

interface ActivityRepository extends CrudRepository<Activity, Long> {

    List<Activity> findAllByUserIdAndType(Long userId, ActivityType type)

    Activity findFirstByUserIdAndTypeOrderByCreatedDesc(Long userId, ActivityType type)

}
