package tbt.app.repositories.challenge

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import tbt.app.domains.challenge.ChallengeVideo
import tbt.app.domains.challenge.ChallengeVideoState

interface ChallengeVideoRepository extends CrudRepository<ChallengeVideo, Long>, PagingAndSortingRepository<ChallengeVideo, Long> {

    List<ChallengeVideo> findAllByUserIdAndStateOrderByCreatedDesc(Long userId, ChallengeVideoState state)

    Page<ChallengeVideo> findAllByState(ChallengeVideoState state, Pageable pageable)

}
