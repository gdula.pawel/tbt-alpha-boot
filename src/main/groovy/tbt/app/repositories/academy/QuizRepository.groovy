package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Quiz

interface QuizRepository extends CrudRepository<Quiz, Long> {

    List<Quiz> findAllByOrderByName()

}
