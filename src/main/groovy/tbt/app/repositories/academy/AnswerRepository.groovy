package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Answer

interface AnswerRepository extends CrudRepository<Answer, Long> {
}
