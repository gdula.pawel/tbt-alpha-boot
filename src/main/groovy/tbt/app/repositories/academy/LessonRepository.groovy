package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Lesson

interface LessonRepository extends CrudRepository<Lesson, Long> {

    List<Lesson> findAllByTags_Id(Long tagId)

}
