package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Course
import tbt.app.domains.academy.Quiz
import tbt.app.domains.skill.Skill

interface CourseRepository extends CrudRepository<Course, Long> {

    Course findBySkill(Skill skill)

    List<Course> findAllByQuiz(Quiz quiz)
}
