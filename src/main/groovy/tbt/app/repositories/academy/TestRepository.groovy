package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Test

interface TestRepository extends CrudRepository<Test, Long> {

}