package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.QuizResult
import tbt.app.domains.skill.Skill

interface QuizResultRepository extends CrudRepository<QuizResult, Long> {

    QuizResult findByUserIdAndSkill(Long userId, Skill skill)

    List<QuizResult> findAllByUserId(Long userId)

}
