package tbt.app.repositories.academy

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.academy.Question

interface QuestionRepository extends CrudRepository<Question, Long> {
}
