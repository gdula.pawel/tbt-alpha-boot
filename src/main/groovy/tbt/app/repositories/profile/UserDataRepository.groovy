package tbt.app.repositories.profile

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.profile.UserData

/**
 * Created by KKACHEL on 17.12.2017.
 */
interface UserDataRepository extends CrudRepository<UserData, Long> {

    UserData findByUserId(Long userId)

}