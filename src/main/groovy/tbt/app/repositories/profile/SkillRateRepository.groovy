package tbt.app.repositories.profile

import org.springframework.data.repository.CrudRepository
import tbt.app.domains.profile.RateType
import tbt.app.domains.profile.SkillRate
import tbt.app.domains.skill.Skill
import tbt.app.domains.user.User

/**
 * Created by KKACHEL on 07.12.2017.
 */
interface SkillRateRepository extends CrudRepository<SkillRate, Long> {

    SkillRate findByUserIdAndTypeAndSkill(Long userId, RateType type, Skill skill)

    List<SkillRate> findAllByUserId(Long userId)

}
