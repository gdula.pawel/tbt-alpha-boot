package tbt.app.services.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import tbt.app.domains.user.Role
import tbt.app.domains.user.User
import tbt.app.repositories.user.UserRepository

@Service
class SecurityService {

    @Value('${tbt.security.targetUrl}')
    private String targetUrl

    private final UserRepository userRepository

    SecurityService(UserRepository userRepository) {
        this.userRepository = userRepository
    }

    boolean isLoggedIn() {
        return principal != "anonymousUser" && role == Role.USER
    }

    Long getLoggedInUserId() {
        getUserId()
    }

    User getLoggedInUser() {
        final Long userId = getUserId()
        return userId == null ? null : userRepository.findOne(userId)
    }

    String targetUrl() {
        return targetUrl
    }

    private String getPrincipal() {
        return (String) SecurityContextHolder?.getContext()?.getAuthentication()?.getPrincipal()
    }

    private Long getUserId() {
        return isLoggedIn() ? Long.valueOf(principal) : null
    }

    private Role getRole() {
        def role = SecurityContextHolder?.getContext()?.getAuthentication()?.authorities
        role ? Role.valueOf(role[0].toString()) : null
    }

}
