package tbt.app.services.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.social.connect.Connection
import org.springframework.social.connect.ConnectionSignUp
import org.springframework.social.facebook.api.Facebook
import org.springframework.social.facebook.api.User as FBUser
import org.springframework.stereotype.Service
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.util.FileUtils
import tbt.app.common.util.ReferralUtils
import tbt.app.domains.activity.content.AccountCreatedV1
import tbt.app.domains.activity.content.ReferralUserRegisteredV1
import tbt.app.domains.mail.RegistrationMail
import tbt.app.domains.user.Profile
import tbt.app.domains.user.User
import tbt.app.repositories.user.ProfileRepository
import tbt.app.repositories.user.UserRepository
import tbt.app.services.activity.ActivityService
import tbt.app.services.mail.MailService

import javax.transaction.Transactional

@Service
class FacebookConnectionSignupService implements ConnectionSignUp {

    private static final Logger LOG = LoggerFactory.getLogger(FacebookConnectionSignupService.class)

    private static final String[] FIELDS = [
            "id", "about", "age_range",
            "birthday", "context", "cover",
            "currency", "devices", "education",
            "email", "favorite_athletes",
            "favorite_teams", "first_name", "gender",
            "hometown", "inspirational_people",
            "installed", "install_type", "is_verified",
            "languages", "last_name", "link", "locale",
            "location", "meeting_for", "middle_name",
            "name", "name_format", "political",
            "quotes", "payment_pricepoints", "relationship_status",
            "religion", "security_settings", "significant_other",
            "sports", "test_group", "timezone", "third_party_id",
            "updated_time", "verified", "video_upload_limits",
            "viewer_can_send_gift", "website", "work"
    ]


    private final ProfileRepository profileRepository

    private final UserRepository userRepository

    private final FileManagerFactory fileManagerFactory

    private final MailService mailService

    private final ActivityService activityService

    FacebookConnectionSignupService(ProfileRepository profileRepository,
                                    UserRepository userRepository,
                                    FileManagerFactory fileManagerFactory,
                                    MailService mailService,
                                    ActivityService activityService) {
        this.profileRepository = profileRepository
        this.userRepository = userRepository
        this.fileManagerFactory = fileManagerFactory
        this.mailService = mailService
        this.activityService = activityService
    }

    @Override
    @Transactional
    String execute(final Connection<?> connection) {
        final Facebook api = (Facebook) connection.getApi()
        final FBUser fbUser = api.fetchObject("me", FBUser.class, FIELDS)
        return getUser(fbUser).id.toString()
    }

    private User getUser(final FBUser fbUser) {
        User user = findByFacebookId(fbUser)

        if (user != null) {
            LOG.info("User with facebook id {} already exists as {}", fbUser.getId(), user.id)
            return user
        }

        LOG.info("User with facebook id {} doesn't exist, crating connection ...")
        user = saveUser(fbUser, saveProfile(fbUser))
        confirmationEmail(user)
        // activities
        referralActivity(user)
        accountCreatedActivity(user)
        user
    }

    private void referralActivity(final User user) {
        if (user.referringUserId) {
            activityService.append(user.id, new ReferralUserRegisteredV1(
                    source: ReferralUserRegisteredV1.Source.FACEBOOK,
                    registeredUserId: user.id,
                    referringUserId: user.referringUserId
            ))
        }
    }

    private void accountCreatedActivity(final User user) {
        activityService.append(user.id, new AccountCreatedV1(
                source: AccountCreatedV1.Source.FACEBOOK,
                userId: user.id,
        ))
    }

    private void confirmationEmail(final User user) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            void afterCommit() {
                mailService.send(new RegistrationMail(user.profile, LocaleContextHolder.getLocale()))
            }
        })
    }

    private Profile saveProfile(final FBUser fbUser) {
        // basic data
        Profile profile = new Profile()
        profile.firstName = fbUser.getFirstName()
        profile.lastName = fbUser.getLastName()
        profile.email = fbUser.getEmail()
        profile.gender = fbUser.getGender()
        profile.ageRangeMin = fbUser.getAgeRange().getMin()
        profile.ageRangeMax = fbUser.getAgeRange().getMax()
        profile = profileRepository.save(profile)
        // avatar
        profile.avatar = saveAvatar(profile.id, fbUser)
        // save changes
        profileRepository.save(profile)

    }

    private User saveUser(final FBUser fbUser, final Profile profile) {
        User user = new User()
        user.externalId = fbUser.getId()
        user.referringUserId = ReferralUtils.get()
        user.profile = profile
        userRepository.save(user)
    }

    private User findByFacebookId(final FBUser fbUser) {
        return userRepository.findByExternalId(fbUser.getId())
    }

    private String saveAvatar(final Long profileId, final FBUser fbUser) {
        def url = "https://graph.facebook.com/${fbUser.getId()}/picture?width=1000&height=1000"
        def image = FileUtils.download(url, "jpg")
        def fileName = FileUtils.generateName(profileId, "jpg")
        fileManagerFactory.get().upload(fileName, FileType.AVATAR, "image/jpg", image)
        fileName
    }

}
