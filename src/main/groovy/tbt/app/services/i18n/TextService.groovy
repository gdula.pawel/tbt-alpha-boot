package tbt.app.services.i18n

import org.springframework.stereotype.Service
import tbt.app.domains.i18n.Text
import tbt.app.repositories.i18n.TextRepository

import javax.transaction.Transactional

@Service
class TextService {

    private final TextRepository textRepository

    TextService(TextRepository textRepository) {
        this.textRepository = textRepository
    }

    @Transactional
    List<Text> save(final List<Text> texts, final Locale locale, final String value) {
        def text = texts?.find { it.locale == locale } ?: new Text(locale: locale)
        def created = text.id == null
        // store data
        text.content = value
        text = textRepository.save(text)

        switch (true) {
            case !texts:
                return [text]
            case created:
                return texts << text
            default:
                return texts
        }
    }

}
