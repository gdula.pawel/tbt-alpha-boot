package tbt.app.services.profile

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service
import reactor.bus.Event
import reactor.bus.EventBus
import tbt.app.common.bus.AbstractConsumer
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ChallengeVideoScoredV1

@Slf4j
@Service
class SkillRateCertificationScoreChangeService extends AbstractConsumer<Event<Activity>> {

    private final SkillRateService skillRateService

    SkillRateCertificationScoreChangeService(EventBus eventBus, SkillRateService skillRateService) {
        super(eventBus)
        this.skillRateService = skillRateService
    }

    @Override
    String getKey() {
        return "activity"
    }

    @Override
    void accept(final Event<Activity> event) {
        if (event.data.type != ActivityType.CHALLENGE_VIDEO_SCORED) {
            log.debug('Activity {} not supported for keeping user score', event.data.type)
            return
        }

        ChallengeVideoScoredV1 content = event.data.content(ChallengeVideoScoredV1)
        skillRateService.saveTBTSkillRate(event.data.userId, content.skillId, content.score)
    }

}
