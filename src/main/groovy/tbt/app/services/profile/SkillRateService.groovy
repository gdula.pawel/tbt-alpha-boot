package tbt.app.services.profile

import org.springframework.stereotype.Service
import tbt.app.domains.activity.content.ProfileRatingUpdatedV1
import tbt.app.domains.profile.RateType
import tbt.app.domains.profile.SkillRate
import tbt.app.domains.skill.Skill
import tbt.app.domains.user.User
import tbt.app.repositories.profile.SkillRateRepository
import tbt.app.repositories.user.UserRepository
import tbt.app.services.activity.ActivityService
import tbt.app.services.security.SecurityService
import tbt.app.services.skills.SkillsService

import javax.transaction.Transactional

import static tbt.app.domains.activity.content.ProfileRatingUpdatedV1.Change

/**
 * Created by KKACHEL on 07.12.2017.
 */
@Service
class SkillRateService {

    private final SkillRateRepository skillRateRepository

    private final SkillsService skillsService

    private final SecurityService securityService

    private final ActivityService activityService

    private final UserRepository userRepository

    SkillRateService(SkillRateRepository skillRateRepository, SkillsService skillsService,
                     SecurityService securityService,
                     ActivityService activityService,
                     UserRepository userRepository) {
        this.skillRateRepository = skillRateRepository
        this.skillsService = skillsService
        this.securityService = securityService
        this.activityService = activityService
        this.userRepository = userRepository
    }

    @Transactional
    void saveUserSkillRate(final Map<String, Integer> data) {
        List<Change> changes = data.collect(this.&saveAndReportChange).grep()
        activityService.append(new ProfileRatingUpdatedV1(changes: changes))
    }


    @Transactional
    void saveTBTSkillRate(final Long userId, final Long skillId, final Integer rate) {
        Skill skill = skillsService.getSkill(skillId)
        SkillRate skillRate = findOrCreate(userId, RateType.TBT_RATE, skill)
        skillRate.rate = rate
        skillRateRepository.save(skillRate)
    }

    Map<RateType, Map<String, Integer>> getGroupedSkillsByTypeForCurrentUser() {
        User loggedUser = securityService.getLoggedInUser()
        Map<RateType, Map<String, Integer>> groupedSkills = skillRateRepository.findAllByUserId(loggedUser.id).groupBy { it.type }.collectEntries {
            [(it.key), it.value.collectEntries { [it.skill.code, it.rate] }]
        }
        return groupedSkills
    }

    private Change saveAndReportChange(final String skillCode, final Integer rate) {
        Long userId = securityService.getLoggedInUserId()
        Skill skill = skillsService.getSkill(skillCode)
        SkillRate skillRate = findOrCreate(userId, RateType.USER_RATE, skill)
        Change change = isRateChanged(skillRate, rate) ? new Change(skillId: skill.id, oldValue: skillRate.rate, newValue: rate) : null
        skillRate.rate = rate
        skillRateRepository.save(skillRate)
        change
    }

    private isRateChanged(final SkillRate skillRate, final Integer newValue) {
        skillRate.rate != newValue && newValue != 0
    }

    private findOrCreate(final Long userId, final RateType type, Skill skill) {
        find(userId, type, skill) ?: create(userId, type, skill)
    }

    private SkillRate find(final Long userId, final RateType type, Skill skill) {
        skillRateRepository.findByUserIdAndTypeAndSkill(userId, type, skill)
    }

    private SkillRate create(final Long userId, final RateType type, Skill skill) {
        new SkillRate(type: type, skill: skill, userId: userId)
    }
}
