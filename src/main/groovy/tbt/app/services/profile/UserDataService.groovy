package tbt.app.services.profile

import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service
import tbt.app.common.util.AppConstantsUtils
import tbt.app.domains.activity.content.ProfileInfoUpdatedV1
import tbt.app.domains.profile.BestLegType
import tbt.app.domains.profile.PositionType
import tbt.app.domains.profile.UserData
import tbt.app.domains.user.User
import tbt.app.repositories.profile.UserDataRepository
import tbt.app.services.activity.ActivityService
import tbt.app.services.security.SecurityService

import javax.transaction.Transactional

/**
 * Created by KKACHEL on 17.12.2017.
 */
@Service
class UserDataService {

    private static final List<String> TRACKED_FIELDS = ['position', 'bestLeg', 'weight', 'growth', 'dateOfBirth', 'teamName', 'teamLeague', 'playerNumber']

    private final UserDataRepository userDataRepository

    private final SecurityService securityService

    private final ActivityService activityService

    UserDataService(UserDataRepository userDataRepository,
                    SecurityService securityService,
                    ActivityService activityService) {
        this.userDataRepository = userDataRepository
        this.securityService = securityService
        this.activityService = activityService
    }

    UserData getUserData(final Long id) {
        return userDataRepository.findByUserId(id)
    }

    UserData getUserData() {
        User loggedUser = securityService.getLoggedInUser()
        return userDataRepository.findByUserId(loggedUser.id)
    }

    @Transactional
    void saveUserData(Map data) {
        UserData userData = getUserData()

        if (!userData) {
            User loggedUser = securityService.getLoggedInUser()
            userData = new UserData()
            userData.userId = loggedUser.id
        }

        createActivities(userData, data)
        setUserDataFields(userData, data)

        userDataRepository.save(userData)
    }

    private void setUserDataFields(UserData userData, Map data) {
        userData.position = PositionType.valueOf(data.get("position"))
        userData.bestLeg = BestLegType.valueOf(data.get("bestLeg"))
        userData.weight = StringUtils.isEmpty(data.get("weight")) ? null : Integer.valueOf(data.get("weight"))
        userData.growth = StringUtils.isEmpty(data.get("growth")) ? null : Integer.valueOf(data.get("growth"))
        userData.dateOfBirth = StringUtils.isEmpty(data.get("dateOfBirth")) ? null : AppConstantsUtils.DATE_FORMAT.parse(data.get("dateOfBirth"))
        userData.teamName = StringUtils.isEmpty(data.get("teamName")) ? null : data.get("teamName")
        userData.teamLeague = StringUtils.isEmpty(data.get("teamLeague")) ? null : data.get("teamLeague")
        userData.playerNumber = StringUtils.isEmpty(data.get("playerNumber")) ? null : Integer.valueOf(data.get("playerNumber"))
    }

    private void createActivities(final UserData userData, final Map data) {
        UserData newUserData = new UserData()
        setUserDataFields(newUserData, data)
        createActivity(userData, newUserData)
    }

    private void createActivity(final UserData oldValues, final UserData newValues) {
        def changes = TRACKED_FIELDS.collect { String fieldName ->
            def oldValue = oldValues."${fieldName}"
            def newValue = newValues."${fieldName}"
            oldValue != newValue ? new ProfileInfoUpdatedV1.Change(name: fieldName, oldValue: oldValue, newValue: newValue) : null
        }.grep()
        activityService.append(new ProfileInfoUpdatedV1(changes: changes))
    }

}
