package tbt.app.services.challenge

import groovy.util.logging.Slf4j
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.thymeleaf.util.Validate
import tbt.app.common.file.FileManager
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.file.VideoDto
import tbt.app.domains.activity.content.ChallengeVideoScoredV1
import tbt.app.domains.activity.content.ChallengeVideoUploadedV1
import tbt.app.domains.challenge.ChallengeVideo
import tbt.app.domains.challenge.ChallengeVideoContext
import tbt.app.domains.challenge.ChallengeVideoState
import tbt.app.repositories.challenge.ChallengeVideoRepository
import tbt.app.services.academy.CourseService
import tbt.app.services.activity.ActivityService
import tbt.app.services.security.SecurityService

import javax.transaction.Transactional

@Slf4j
@Service
class ChallengeVideoService {

    private final SecurityService securityService

    private final ActivityService activityService

    private final ChallengeVideoRepository challengeRepository

    private final FileManagerFactory fileManagerFactory

    private final CourseService courseService

    ChallengeVideoService(SecurityService securityService,
                          ActivityService activityService,
                          ChallengeVideoRepository challengeRepository,
                          FileManagerFactory fileManagerFactory,
                          CourseService courseService) {
        this.securityService = securityService
        this.activityService = activityService
        this.challengeRepository = challengeRepository
        this.fileManagerFactory = fileManagerFactory
        this.courseService = courseService
    }

    ChallengeVideo get(final Long id) {
        challengeRepository.findOne(id)
    }

    Page<ChallengeVideo> list(final PageRequest pageRequest) {
        challengeRepository.findAllByState(ChallengeVideoState.UPLOAD_CONFIRMED, pageRequest)
    }

    @Transactional
    ChallengeVideo init(final Long courseId, final String fileName) {
        Validate.notNull(courseId, "Course can't be null")
        Validate.notNull(fileName, "Video can't be null")
        final def user = securityService.loggedInUser
        final def course = courseService.get(courseId)
        final def challengeVideo = challengeRepository.save(new ChallengeVideo(
                name: fileName,
                user: user,
                context: ChallengeVideoContext.ACADEMY,
                course: course,
                state: ChallengeVideoState.UPLOAD_INITIALIZED
        ))
        challengeVideo
    }

    @Transactional
    ChallengeVideo confirm(final Long id) {
        Validate.notNull(id, "Id can't be null")
        def challengeVideo = challengeRepository.findOne(id)
        challengeVideo.state = ChallengeVideoState.UPLOAD_CONFIRMED
        challengeVideo.confirmed = new Date()
        challengeVideo = challengeRepository.save(challengeVideo)
        activityService.append(new ChallengeVideoUploadedV1(
                context: challengeVideo.context,
                videoId: challengeVideo.id,
                courseId: challengeVideo.course.id,
                skillId: challengeVideo.course.skill.id
        ))
        challengeVideo
    }

    @Transactional
    ChallengeVideo saveScore(final Long id, final Integer score) {
        Validate.notNull(id, "Id can't be null")
        Validate.notNull(score, "Score can't be null")
        def challengeVideo = get(id)
        challengeVideo.score = score
        challengeVideo = challengeRepository.save(challengeVideo)
        activityService.append(challengeVideo.user.id, new ChallengeVideoScoredV1(
                context: challengeVideo.context,
                videoId: challengeVideo.id,
                courseId: challengeVideo.course.id,
                skillId: challengeVideo.course.skill.id,
                score: score
        ))
        return challengeVideo
    }

    @Transactional
    ChallengeVideo update(final Long id, final Closure binder) {
        Validate.notNull(id, "ChallengeVideo can't be null")
        Validate.notNull(binder, "Binder can't be null")
        ChallengeVideo video = get(id)
        binder(video)
        challengeRepository.save(video)
    }

    List<VideoDto> getAllCurrentUserVideos() {
        final Long userId = securityService.loggedInUserId
        final FileManager manager = fileManagerFactory.get()
        challengeRepository.findAllByUserIdAndStateOrderByCreatedDesc(userId, ChallengeVideoState.UPLOAD_CONFIRMED).collect {
            new VideoDto(
                    it.id,
                    "skill.${it.course.skill.code}".toLowerCase(),
                    manager.getPath(it.name, FileType.VIDEO),
                    manager.getPath(it.poster, FileType.VIDEO_POSTER)
            )
        }
    }

}
