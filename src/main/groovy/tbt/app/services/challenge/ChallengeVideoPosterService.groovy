package tbt.app.services.challenge

import groovy.util.logging.Slf4j
import org.bytedeco.javacv.FFmpegFrameGrabber
import org.bytedeco.javacv.Frame
import org.bytedeco.javacv.Java2DFrameConverter
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import reactor.bus.Event
import reactor.bus.EventBus
import tbt.app.common.bus.AbstractConsumer
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.util.FileUtils
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ChallengeVideoUploadedV1
import tbt.app.domains.challenge.ChallengeVideo

import javax.imageio.ImageIO

@Slf4j
@Service
class ChallengeVideoPosterService extends AbstractConsumer<Event<Activity>> {

    @Value('${tbt.poster.generation.enabled}')
    private boolean enabled

    private final FileManagerFactory fileManagerFactory

    private final ChallengeVideoService challengeService

    ChallengeVideoPosterService(EventBus eventBus,
                                FileManagerFactory fileManagerFactory,
                                ChallengeVideoService challengeService) {
        super(eventBus)
        this.fileManagerFactory = fileManagerFactory
        this.challengeService = challengeService
    }

    @Override
    protected String getKey() {
        return "activity"
    }

    @Override
    void accept(final Event<Activity> event) {
        if (event.data.type != ActivityType.CHALLENGE_VIDEO_UPLOADED) {
            log.debug('Activity {} not supported for keeping user score', event.data.type)
            return
        }

        if (!enabled) {
            log.info('Poster generation disabled')
            return
        }

        try {
            // prepare basic data
            final ChallengeVideoUploadedV1 eventContent = event.data.content(ChallengeVideoUploadedV1)
            log.info('Starting generation of the poster for ChallengeVideo {}', eventContent.videoId)
            final ChallengeVideo video = challengeService.get(eventContent.videoId)
            final String videoUrl = fileManagerFactory.get().getPath(video.name, FileType.VIDEO)
            final InputStream videoStream = new URL(videoUrl).openStream()
            // grab frame
            final Java2DFrameConverter converter = new Java2DFrameConverter()
            final FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(videoStream)
            grabber.start()
            final Frame frame = grabber.grabImage()
            // clean up stream
            videoStream.close()
            // store captured image
            final String posterName = FileUtils.generateName(event.data.userId, 'jpg')
            final ByteArrayOutputStream os = new ByteArrayOutputStream()
            ImageIO.write(converter.convert(frame), "jpg", os)
            InputStream posterStream = new ByteArrayInputStream(os.toByteArray())
            fileManagerFactory.get().upload(posterName, FileType.VIDEO_POSTER, 'image/jpg', posterStream)
            // update video
            challengeService.update(video.id) { ChallengeVideo it -> it.poster = posterName }
            log.info('End poster generation for ChallengeVideo {}', eventContent.videoId)
        } catch (Exception exp) {
            log.error('There was an error while capturing poster image', exp)
        }
    }
}
