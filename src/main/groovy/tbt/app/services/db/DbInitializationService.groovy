package tbt.app.services.db

import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import tbt.app.domains.academy.Course
import tbt.app.domains.academy.Quiz
import tbt.app.domains.skill.Category
import tbt.app.domains.skill.Skill
import tbt.app.domains.tag.Tag
import tbt.app.domains.user.Role
import tbt.app.repositories.academy.CourseRepository
import tbt.app.repositories.academy.LessonRepository
import tbt.app.repositories.skill.SkillRepository
import tbt.app.services.academy.CourseService
import tbt.app.services.academy.QuizService
import tbt.app.services.tag.TagService
import tbt.cms.commons.utils.I18nCommons
import tbt.cms.controllers.academy.AcademyController
import tbt.cms.controllers.academy.AcademyLessonController
import tbt.cms.domains.user.CmsRole as CmsRole
import tbt.cms.domains.user.CmsUser as CmsUser
import tbt.cms.repositories.user.CmsRoleRepository
import tbt.cms.repositories.user.CmsUserRepository

import javax.transaction.Transactional
import java.util.function.Consumer

@Service
class DbInitializationService implements InitializingBean {

    @Value('${tbt.db.initializeDb}')
    private boolean shouldBeRun

    @Autowired
    private SkillRepository skillRepository

    @Autowired
    private CourseRepository courseRepository

    @Autowired
    private LessonRepository lessonRepository

    @Autowired
    private CmsUserRepository cmsUserRepository

    @Autowired
    private CmsRoleRepository cmsRoleRepository

    @Autowired
    private PasswordEncoder passwordEncoder

    @Autowired
    private CourseService courseService

    @Autowired
    private QuizService quizService

    @Autowired
    private TagService tagService

    @Override
    @Transactional
    void afterPropertiesSet() throws Exception {
        createAdmin()

        if (!shouldBeRun) {
            return
        }

        def tag1 = creteTag('tag 1')
        def tag2 = creteTag('tag 2')
        def tag3 = creteTag('tag 3')
        def tag4 = creteTag('tag 4')
        def tags = [tag1, tag2, tag3, tag4].collect { it.id }.join(',')

        createSkill(Category.PHYSICAL, "POWER_EXPLOSIVITY", "POWER", { skill ->
            final Course course = new Course()
            course.skill = skill
            course = courseRepository.save(course)

            course = courseService.saveLessons(
                    skill.id,
                    Locale.FRANCE,
                    AcademyLessonController.MAX_ALLOWED_LECTION,
                    [
                            'videoid_1': '2T0NvbR1Qa0',
                            'text_1'   : 'Lesson 1',
                            'main_1'   : 'true',
                            'tags_1'   : tags,

                            'videoid_2': 'prgcp6CpQCU',
                            'text_2'   : 'Lesson 2',
                            'main_2'   : 'true',
                            'tags_2'   : tags,

                            'videoid_3': 'ZbTaxxGgcDE',
                            'text_3'   : 'Lesson 3',
                            'tags_3'   : tags,

                            'videoid_4': 'I_IPWXy955M',
                            'text_4'   : 'Lesson 4',
                            'tags_4'   : tags,

                            'videoid_5': '4EtGjN7MwC8',
                            'text_5'   : 'Lesson 5',
                            'tags_5'   : tags

                    ])

            Quiz quiz = quizService.saveQuiz(
                    null,
                    Locale.FRANCE,
                    AcademyController.MAX_ALLOWED_QUESTIONS,
                    AcademyController.MAX_ALLOWED_ANSWERS,
                    [
                            'name'          : 'Example quiz',

                            'question_1'    : 'L’amélioration de l’explosivité passe par',
                            'answer_1_1'    : 'L’acquisition d’un gros volume musculaire',
                            'answer_1_2'    : 'La répétition de sprints longs',
                            'answer_1_3'    : 'La répétition de frappes de balles',
                            'answer_1_4'    : 'Un travail de feintes',
                            'answer_1_5'    : 'Un travail de renforcement musculaire',
                            'valid_answer_1': '1',

                            'question_2'    : 'Un joueur puissant/explosif',
                            'answer_2_1'    : 'Court vite',
                            'answer_2_2'    : 'Saute haut et loin',
                            'answer_2_3'    : 'Tire fort',
                            'valid_answer_2': '2',

                            'question_3'    : 'Le renforcement musculaire de la puissance/explosivité concerne',
                            'answer_3_1'    : 'Les mollets',
                            'answer_3_2'    : 'Les quadriceps',
                            'answer_3_3'    : 'Tous les muscles du membre inférieur',
                            'answer_3_4'    : 'Tous les muscles du membre inférieur et le gainage',
                            'valid_answer_3': '3',
                    ]
            )

            course.quiz = quiz
            courseRepository.save(course)

            courseService.saveTest(skill.id, Locale.FRANCE, [
                    explanationVideoId: 'H5tr4cgCHfY',
                    explanationImageId: '1',
                    expectedTime      : '5',
                    videoTitle        : 'Test de vitesse-coordination en navette',
                    requirements      : """
                        <li class="title">Material necessaire</li>
                        <li class="item">1 terrain de foot avec lignes tracées</li>
                        <li class="item">1 smartphone</li>
                    """,
                    testExplanation   : """
                        Départ arrêté derrière la ligne des 6 mètres (5.5 mètres). <b>Réaliser 5 aller-retour le plus
                        rapidement possible</b> entre la ligne des 6 mètres et la ligne de but. A chaque revirement, la
                        pose du
                        pied se réalise au-delà de la ligne.
                    """,
                    videoExplanation  : """
                        <b>Plan vidéo</b> : Suffisamment proche, permettant de vérifier la pose du pied se réalise
                        au-delà de la
                        ligne à chaque point de revirement.
                    """,
                    scoreCalculation  : """
                        Le score au test coïncide avec le temps mis pour réaliser les 5 aller-retour.
                    """
            ])

        })

        createSkill(Category.PHYSICAL, "POWER_EXPLOSIVITY", "EXPLOSIVITY", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "SPEED", "GENERAL_SPEED", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "SPEED", "ACCELERATION", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "SPEED", "GESTURAL_SPEED", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "", "ENDURANCE", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "", "BALANCED", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "", "COORDINATION", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "SHOOTING_MASTERY", "PRECISION", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "FLEXIBILITY", "BACKBONE", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "FLEXIBILITY", "HAMSTRINGS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.PHYSICAL, "FLEXIBILITY", "ADDUCTORS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "SHOOTING_MASTERY", "BRUSH", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "", "SHOT_POWER", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "JUGGLING", "STATIC", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "JUGGLING", "DYNAMIC", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "PASS_ACCURACY", "GROUND_PASS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "PASS_ACCURACY", "AIR_PASS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "", "BALL_DRIVING", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "", "BALL_CONTROL", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.TECHNICAL, "", "DRIBBLE", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "PERSONALITY_FACTORS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "MENTAL_STRENGTH_FACTORS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "EMOTIONAL_INTELLIGENCE", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "ANXIETY_CONTROL", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "ATTENTIONAL_AVAILABILITY", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.MENTAL, "", "ATTENTIONAL_SHARING", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.META, "", "THE_PATH_FOR_BEING_PRO_FOOTBALLER", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
        createSkill(Category.META, "", "THE_RIGHT_FOOTBALLER_LIFESTYLE", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })

        createSkill(Category.FREESTYLE, "", "VARIETY_OF_TRICKS", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })

        createSkill(Category.FREESTYLE, "", "QUALITY_OF_EXECUTION", { skill ->
            final Course course = new Course()
            course.skill = skill
            courseRepository.save(course)
        })
    }

    private void createAdmin() {
        def userName = "admin"
        def userPassword = "/,!6|v5-!wmdLG"

        if (!cmsUserRepository.findByUsername(userName)) {
            cmsRoleRepository.save(new CmsRole(username: userName, role: Role.ADMIN.name()))
            cmsUserRepository.save(new CmsUser(username: userName, password: passwordEncoder.encode(userPassword), enabled: true))
        }
    }

    private void createSkill(
            final Category category, final categoryGroup, final String code, final Consumer<Skill> function) {
        if (skillRepository.findByCode(code) != null) {
            return
        }

        final Skill skill = new Skill()
        skill.category = category
        skill.categoryGroup = categoryGroup
        skill.code = code
        final Skill entity = skillRepository.save(skill)
        function.accept(entity)
    }

    private Tag creteTag(final String name) {
        tagService.list().find { it.name == name } ?: tagService.save(null, name, I18nCommons.defaultLocale)
    }

}
