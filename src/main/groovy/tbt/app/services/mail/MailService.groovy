package tbt.app.services.mail

import org.apache.commons.lang.Validate
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import tbt.app.domains.mail.ContactMail
import tbt.app.domains.mail.RegistrationMail

import javax.mail.internet.MimeMessage
import java.nio.charset.StandardCharsets

@Service
class MailService {

    private static final String FROM = "contact@thebigteam.co"

    private static final String CONTACT_TO = "contact@thebigteam.co"

    private static final String[] CONTACT_BCC = ['gdula.pawel@gmail.com'] as String[]

    private final JavaMailSender sender

    private final MessageSource messageSource

    @Value('${mail.registration.domain}')
    private final String domain

    MailService(JavaMailSender sender, MessageSource messageSource) {
        this.sender = sender
        this.messageSource = messageSource
    }

    @Async
    void send(ContactMail mail) {
        Validate.notNull(mail, "Mail can't be null")
        SimpleMailMessage message = new SimpleMailMessage()
        message.from = FROM
        message.to = CONTACT_TO
        message.bcc = CONTACT_BCC
        message.subject = "Contact request from ${mail.from} over thebest.team"
        message.text = """From: ${mail.from}\nBody:\n${mail.body}"""
        sender.send(message)
    }

    @Async
    void send(final RegistrationMail mail) {
        Validate.notNull(mail, "Mail can't be null")
        MimeMessage mimeMessage = sender.createMimeMessage()
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name())
        helper.from = FROM
        helper.to = mail.profile.email
        helper.subject = messageSource.getMessage("mail.registration.subject", [] as Object[], mail.locale)
        helper.setText(getRegistrationMailBody(mail), true)
        sender.send(mimeMessage)
    }

    private String getRegistrationMailBody(final RegistrationMail mail) {
        messageSource.getMessage("mail.registration.body", [mail.profile.firstName, domain] as Object[], mail.locale)
    }

}
