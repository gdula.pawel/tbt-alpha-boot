package tbt.app.services.activity

import com.google.gson.Gson
import org.springframework.stereotype.Service
import org.springframework.transaction.support.TransactionSynchronizationAdapter
import org.springframework.transaction.support.TransactionSynchronizationManager
import org.thymeleaf.util.Validate
import tbt.app.common.bus.Publisher
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ActivityContent
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.security.SecurityService

import javax.transaction.Transactional

@Service
class ActivityService extends TransactionSynchronizationAdapter {

    private final ActivityRepository activityRepository

    private final SecurityService securityService

    private final Publisher publisher

    private final Gson gson

    ActivityService(ActivityRepository eventRepository,
                    SecurityService securityService,
                    Publisher publisher,
                    Gson gson) {
        this.activityRepository = eventRepository
        this.securityService = securityService
        this.publisher = publisher
        this.gson = gson
    }

    @Transactional
    Activity append(final ActivityContent content) {
        append(content.type()) { Activity activity ->
            activity.version = content.version()
            activity.body = content.type().encode(content)
        }
    }

    @Transactional
    Activity append(Long userId, final ActivityContent content) {
        append(userId, content.type()) { Activity activity ->
            activity.version = content.version()
            activity.body = content.type().encode(content)
        }
    }

    @Transactional
    Activity append(ActivityType eventType, Closure binder = null) {
        User user = securityService.getLoggedInUser()
        append(user.id, eventType, binder)
    }

    @Transactional
    Activity append(Long userId, ActivityType eventType, Closure binder = null) {
        Validate.notNull(userId, "User id can't be null")
        Validate.notNull(eventType, "Action can't be null")
        final Activity event = new Activity(type: eventType, userId: userId)

        if (binder) {
            binder(event)
        }

        save(event)
    }

    @Transactional
    boolean appendIfNotExist(final ActivityType eventType) {
        Validate.notNull(eventType, "Action can't be null")
        final User user = securityService.getLoggedInUser()

        if (hasActivity(eventType)) {
            return false
        }

        save(new Activity(type: eventType, userId: user.id))
        return true
    }

    @Transactional
    boolean hasActivity(final ActivityType eventType) {
        final User user = securityService.getLoggedInUser()
        activityRepository.findFirstByUserIdAndTypeOrderByCreatedDesc(user.id, eventType) != null
    }

    @Transactional
    Activity getLastOne(final ActivityType eventType) {
        final User user = securityService.getLoggedInUser()
        getLastOne(user.id, eventType)
    }

    @Transactional
    Activity getLastOne(final Long userId, final ActivityType eventType) {
        activityRepository.findFirstByUserIdAndTypeOrderByCreatedDesc(userId, eventType)
    }

    @Transactional
    List<Activity> getAll(final Long userId, final ActivityType eventType) {
        activityRepository.findAllByUserIdAndType(userId, eventType)
    }

    private Activity save(final Activity activity) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            void afterCommit() {
                publisher.publish(activity)
            }
        })
        activityRepository.save(activity)
    }

}
