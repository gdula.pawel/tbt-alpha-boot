package tbt.app.services.score

import tbt.app.domains.activity.ActivityType
import tbt.app.domains.user.User

interface PointsCalculator {

    Integer calculate(User user)

    boolean baseOn(ActivityType activityType)

}
