package tbt.app.services.score

import groovy.util.logging.Slf4j
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.score.Score
import tbt.app.domains.user.User
import tbt.app.repositories.user.UserRepository

@Slf4j
@Service
class ScoreService {

    private final List<PointsCalculator> pointsCalculators

    private final UserRepository userRepository

    ScoreService(List<PointsCalculator> pointsCalculators, UserRepository userRepository) {
        this.pointsCalculators = pointsCalculators
        this.userRepository = userRepository
    }

    @Cacheable("score")
    Score get(final Long userId) {
        calculate(userId)
    }

    @CacheEvict("score")
    Score refreshAndGet(final Long userId) {
        calculate(userId)
    }

    private Score calculate(final Long userId) {
        User user = userRepository.findOne(userId)
        Integer points = pointsCalculators.sum { PointsCalculator it -> it.calculate(user) } as Integer ?: 0
        log.info('Score {} calculated for user {}', points, userId)
        return new Score(points)
    }

    boolean isScoreChanged(final ActivityType activityType) {
        pointsCalculators.find { it.baseOn(activityType) } != null
    }

}
