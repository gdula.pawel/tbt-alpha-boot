package tbt.app.services.score

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service
import reactor.bus.Event
import reactor.bus.EventBus
import tbt.app.common.bus.AbstractConsumer
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ScoreChangedV1
import tbt.app.domains.score.Score
import tbt.app.services.activity.ActivityService

@Slf4j
@Service
class ScoreHistoryService extends AbstractConsumer<Event<Activity>> {

    private final ScoreService scoreService

    private final ActivityService activityService

    ScoreHistoryService(EventBus eventBus,
                        ScoreService scoreService,
                        ActivityService activityService) {
        super(eventBus)
        this.scoreService = scoreService
        this.activityService = activityService
    }

    @Override
    String getKey() {
        return "activity"
    }

    @Override
    void accept(final Event<Activity> event) {
        if (!scoreService.isScoreChanged(event.data.type)) {
            log.debug('Activity {} not supported in score recalculation', event.data.type)
            return
        }

        Integer points = scoreService.refreshAndGet(event.data.userId).points
        Integer lastCalculated = activityService.getLastOne(event.data.userId, ActivityType.SCORE_CHANGED)?.content(ScoreChangedV1)?.points ?: 0

        if (points == lastCalculated) {
            log.info('No points change (current {}) for user {}', points, event.data.userId)
            return
        }


        activityService.append(event.data.userId, new ScoreChangedV1(
                trigger: event.data.type,
                triggerId: event.data.id,
                points: points,
                delta: (points - lastCalculated)
        ))
    }

    List<Score> getPerDay(final Long userId) {
        activityService.getAll(userId, ActivityType.SCORE_CHANGED)
                .groupBy { new Date(it.created.time).clearTime() }
                .collect { new Score(it.value.sort { Activity a -> a.created }.last().content(ScoreChangedV1).points, it.key) }
                .sort { it.date }
    }

    List<Score> getDetailed(final Long userId) {
        activityService
                .getAll(userId, ActivityType.SCORE_CHANGED)
                .collect { new Score(it.content(ScoreChangedV1).points, it.created, it.content(ScoreChangedV1).trigger) }
                .sort { it.date }
                .reverse()
    }

}
