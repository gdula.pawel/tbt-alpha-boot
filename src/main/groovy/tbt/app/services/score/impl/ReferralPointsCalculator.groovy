package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository

@Component
class ReferralPointsCalculator extends AbstractPointsCalculator {

    private static final Integer POINTS = 350

    private final ActivityRepository activityRepository

    ReferralPointsCalculator(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        activityRepository.findAllByUserIdAndType(user.id, ActivityType.REFERRAL_USER_REGISTERED) ? POINTS : 0
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.REFERRAL_USER_REGISTERED == activityType
    }

}
