package tbt.app.services.score.impl

import tbt.app.domains.user.User
import tbt.app.services.score.PointsCalculator

abstract class AbstractPointsCalculator implements PointsCalculator {

    private static final Integer NULL_RESULT = 0

    @Override
    Integer calculate(final User user) {
        user ? (doCalculation(user) ?: NULL_RESULT) : NULL_RESULT
    }

    protected abstract Integer doCalculation(User user)
}
