package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ProfileRatingUpdatedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository

@Component
class ProfileRatingUpdatePointsCalculator extends AbstractPointsCalculator {

    private static final Integer POINTS_FOR_UPDATED_RATING = 10

    private final ActivityRepository activityRepository

    ProfileRatingUpdatePointsCalculator(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        Integer numberOfUpdatedFields = activityRepository
                .findAllByUserIdAndType(user.id, ActivityType.PROFILE_RATING_UPDATED)
                .collect { it.content(ProfileRatingUpdatedV1).changes }
                .flatten()
                .groupBy { ProfileRatingUpdatedV1.Change it -> it.skillId }.size()
        numberOfUpdatedFields * POINTS_FOR_UPDATED_RATING
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.PROFILE_RATING_UPDATED == activityType
    }

}
