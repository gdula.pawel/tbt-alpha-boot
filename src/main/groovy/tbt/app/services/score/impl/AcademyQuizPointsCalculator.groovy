package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.AcademyQuizSubmittedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository

@Component
class AcademyQuizPointsCalculator extends AbstractPointsCalculator {

    private static final Map<Integer, Integer> WEIGHT = [0: 100, 1: 50, 2: 30]

    private static final Integer DEFAULT_WEIGHT = 1

    private static final Integer NORMALIZATOR = 50

    private final ActivityRepository activityRepository

    AcademyQuizPointsCalculator(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        Math.ceil(sumWeightedAvg(user.id) / NORMALIZATOR)
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.ACADEMY_QUIZ_SUBMITTED == activityType
    }

    private Integer sumWeightedAvg(final Long userId) {
        activityRepository
                .findAllByUserIdAndType(userId, ActivityType.ACADEMY_QUIZ_SUBMITTED)
                .groupBy { it.content(AcademyQuizSubmittedV1).courseId }
                .collect(this.&getWeightedAvg)
                .sum() ?: 0
    }

    private Integer getWeightedAvg(final Map.Entry<Long, List<Activity>> entry) {
        Math.ceil(getWeightedSum(entry.value) / entry.value.size())
    }

    private Integer getWeightedSum(List<Activity> activities) {
        activities
                .sort { it.created }
                .withIndex()
                .collect { Activity a, Integer index -> a.content(AcademyQuizSubmittedV1).score * getWeight(index) }
                .sum() as Integer
    }

    private Integer getWeight(final Integer attempt) {
        WEIGHT[attempt] ?: DEFAULT_WEIGHT
    }
}
