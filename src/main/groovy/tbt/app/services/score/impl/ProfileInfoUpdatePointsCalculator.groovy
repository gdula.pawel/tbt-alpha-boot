package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ProfileInfoUpdatedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository

@Component
class ProfileInfoUpdatePointsCalculator extends AbstractPointsCalculator {

    private static final Integer POINTS_FOR_UPDATED_FIELD = 20

    private final ActivityRepository activityRepository

    ProfileInfoUpdatePointsCalculator(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        Integer numberOfUpdatedFields = activityRepository
                .findAllByUserIdAndType(user.id, ActivityType.PROFILE_INFO_UPDATED)
                .collect { it.content(ProfileInfoUpdatedV1).changes }
                .flatten()
                .groupBy { ProfileInfoUpdatedV1.Change it -> it.name }.size()

        numberOfUpdatedFields * POINTS_FOR_UPDATED_FIELD
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.PROFILE_INFO_UPDATED == activityType
    }

}
