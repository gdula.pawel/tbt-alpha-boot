package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ChallengeVideoScoredV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository

@Component
class AcademyTestVideoPointsCalculator extends AbstractPointsCalculator {

    private static final Integer BASE_POINTS = 350

    private static final Integer ADDITIONAL_VIDEOS_POINTS = 50

    private static final Integer MAX_SCORE_POINTS = 50

    private final ActivityRepository activityRepository

    AcademyTestVideoPointsCalculator(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        activityRepository
                .findAllByUserIdAndType(user.id, ActivityType.CHALLENGE_VIDEO_SCORED)
                .groupBy { it.content(ChallengeVideoScoredV1).courseId }
                .collect(this.&process)
                .sum()
    }

    private Integer process(final Map.Entry<String, List<Activity>> entry) {
        BASE_POINTS + maxScorePoints(entry.value) + moreThanOneVideoPoints(entry.value)
    }

    private Integer maxScorePoints(final List<Activity> activities) {
        activities.find { it.content(ChallengeVideoScoredV1).score == 10 } ? MAX_SCORE_POINTS : 0
    }

    private Integer moreThanOneVideoPoints(final List<Activity> activities) {
        activities.size() > 1 ? ADDITIONAL_VIDEOS_POINTS : 0
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.CHALLENGE_VIDEO_SCORED == activityType
    }
}
