package tbt.app.services.score.impl

import org.springframework.stereotype.Component
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.AcademyVideoSeenV1
import tbt.app.domains.user.User
import tbt.app.repositories.academy.LessonRepository
import tbt.app.repositories.activity.ActivityRepository

@Component
class AcademyWatchedVideoPointsCalculator extends AbstractPointsCalculator {

    public static final Integer MAIN_VIDEO_POINTS = 15

    public static final Integer ADDITIONAL_VIDEO_POINTS = 2

    private final ActivityRepository activityRepository

    private final LessonRepository lessonRepository

    AcademyWatchedVideoPointsCalculator(ActivityRepository activityRepository,
                                        LessonRepository lessonRepository) {
        this.activityRepository = activityRepository
        this.lessonRepository = lessonRepository
    }

    @Override
    protected Integer doCalculation(User user) {
        activityRepository
                .findAllByUserIdAndType(user.id, ActivityType.ACADEMY_LESSON_VIDEO_SEEN)
                .unique { it.content(AcademyVideoSeenV1).lessonId }
                .sum this.&sum
    }

    private Integer sum(Activity activity) {
        lessonRepository.findOne(activity.content(AcademyVideoSeenV1).lessonId)?.main ?
                MAIN_VIDEO_POINTS :
                ADDITIONAL_VIDEO_POINTS
    }

    @Override
    boolean baseOn(ActivityType activityType) {
        return ActivityType.ACADEMY_LESSON_VIDEO_SEEN == activityType
    }
}
