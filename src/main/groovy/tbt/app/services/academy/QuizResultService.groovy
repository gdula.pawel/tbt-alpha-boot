package tbt.app.services.academy

import org.springframework.stereotype.Service
import tbt.app.domains.academy.QuizResult
import tbt.app.domains.skill.Skill
import tbt.app.domains.user.User
import tbt.app.repositories.academy.QuizResultRepository
import tbt.app.services.security.SecurityService
import tbt.app.services.skills.SkillsService

import javax.transaction.Transactional

@Service
class QuizResultService {

    private final QuizResultRepository quizResultRepository

    private final SecurityService securityService

    QuizResultService(QuizResultRepository quizResultRepository, SecurityService securityService) {
        this.quizResultRepository = quizResultRepository
        this.securityService = securityService
    }

    @Transactional
    QuizResult saveQuizResult(Skill skill, float score) {
        User loggedUser = securityService.getLoggedInUser()
        QuizResult result = quizResultRepository.findByUserIdAndSkill(loggedUser.id, skill)
        if(result == null) {
            result = new QuizResult()
            result.setUserId(loggedUser.id)
            result.setSkill(skill)
        }
        result.setResult(Math.round(score))
        quizResultRepository.save(result)
    }

    Map<Long, Integer> getGroupedQuizResultsBySkillIdForCurrentUser() {
        User loggedUser = securityService.getLoggedInUser()
        List<QuizResult> results = quizResultRepository.findAllByUserId(loggedUser.id)
        Map<Long, Integer> groupedResult = results.collectEntries {
            [(it.skill.id), it.result]
        }
        return groupedResult
    }

}
