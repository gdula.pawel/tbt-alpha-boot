package tbt.app.services.academy

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.thymeleaf.util.Validate
import tbt.app.common.academy.CourseTestResultDto
import tbt.app.domains.academy.Course
import tbt.app.domains.academy.Quiz
import tbt.app.domains.academy.Test
import tbt.app.domains.activity.content.AcademyQuizSubmittedV1
import tbt.app.repositories.academy.CourseRepository
import tbt.app.repositories.academy.TestRepository
import tbt.app.services.activity.ActivityService
import tbt.app.services.i18n.TextService
import tbt.app.services.skills.SkillsService

import javax.transaction.Transactional

@Slf4j
@Service
class CourseService {

    @Value('${tbt.academy.defaultSkill}')
    private String defaultSkill

    private final SkillsService skillsService

    private final CourseRepository courseRepository

    private final ActivityService activityService

    private final TestRepository testRepository

    private final TextService textService

    private final QuizService quizService

    private final LessonService lessonService

    CourseService(SkillsService skillsService,
                  CourseRepository courseRepository,
                  ActivityService activityService,
                  TestRepository testRepository,
                  TextService textService,
                  QuizService quizService,
                  LessonService lessonService) {
        this.skillsService = skillsService
        this.courseRepository = courseRepository
        this.activityService = activityService
        this.testRepository = testRepository
        this.textService = textService
        this.quizService = quizService
        this.lessonService = lessonService
    }

    Course get(final Long id) {
        courseRepository.findOne(id)
    }

    Course getCourseOrDefault(final Long skillId) {
        def skill = skillsService.getSkillOrDefault(skillId)
        log.info("Selected skill {} with submitted skill id {}", skill.code, skillId)
        courseRepository.findBySkill(skill)
    }

    Course getCourse(final Long skillId) {
        def skill = skillsService.getSkill(skillId)
        skill ? courseRepository.findBySkill(skill) : null
    }

    CourseTestResultDto checkTest(final Course course, final Map<Long, Long> answers) {
        Validate.notNull(course, "Course object can't be null")
        def result = new CourseTestResultDto(course, answers?.collect { it.key })

        if (!result.isAllQuestionsSubmitted()) {
            return result
        }

        course.quiz.questions.each { q ->
            if (q.answers.find { a -> a.id == answers[q.id] }.valid) {
                result.correctAnswers << q.id
            }
        }

        activityService.append(new AcademyQuizSubmittedV1(courseId: course.id, skillId: course.skill.id, score: result.score()))
        return result
    }

    @Transactional
    Test saveTest(final Long skillId, final Locale locale, final Map<String, String> data) {
        def course = getCourse(skillId)
        def test = course?.test ?: new Test()
        // basic data
        test.explanationVideoId = data['explanationVideoId']
        test.explanationImageId = getInteger(data['explanationImageId'])
        test.expectedTime = getInteger(data['expectedTime'])
        // i18n data
        test.videoTitle = textService.save(test.videoTitle, locale, data['videoTitle'])
        test.requirements = textService.save(test.requirements, locale, data['requirements'])
        test.testExplanation = textService.save(test.testExplanation, locale, data['testExplanation'])
        test.videoExplanation = textService.save(test.videoExplanation, locale, data['videoExplanation'])
        test.scoreCalculation = textService.save(test.scoreCalculation, locale, data['scoreCalculation'])
        // save test and course
        course.test = testRepository.save(test)
        courseRepository.save(course)
        // done
        test
    }

    private Integer getInteger(final String data) {
        try {
            data ? Integer.valueOf(data) : null
        } catch (Exception exp) {
            log.warn('Error while converting number', exp)
            null
        }
    }

    @Transactional
    Course saveQuiz(final Long skillId,
                    final Long quizId) {
        Course course = getCourse(skillId)
        Quiz quiz = quizService.get(quizId)
        course.quiz = quiz
        return courseRepository.save(course)
    }

    @Transactional
    Course saveLessons(final Long skillId,
                       final Locale locale,
                       final Range maxLessons,
                       final Map<String, String> data) {
        Course course = getCourse(skillId)
        course.lessons = lessonService.save(course, locale, maxLessons, data)
        return courseRepository.save(course)
    }

}
