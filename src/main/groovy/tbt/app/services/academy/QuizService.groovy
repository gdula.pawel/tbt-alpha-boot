package tbt.app.services.academy

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service
import tbt.app.domains.academy.Answer
import tbt.app.domains.academy.Question
import tbt.app.domains.academy.Quiz
import tbt.app.repositories.academy.AnswerRepository
import tbt.app.repositories.academy.CourseRepository
import tbt.app.repositories.academy.QuestionRepository
import tbt.app.repositories.academy.QuizRepository
import tbt.app.services.i18n.TextService

import javax.transaction.Transactional

@Slf4j
@Service
class QuizService {

    private final TextService textService

    private final QuizRepository quizRepository

    private final QuestionRepository questionRepository

    private final AnswerRepository answerRepository

    private final CourseRepository courseRepository

    QuizService(TextService textService,
                QuizRepository quizRepository,
                QuestionRepository questionRepository,
                AnswerRepository answerRepository,
                CourseRepository courseRepository) {
        this.textService = textService
        this.questionRepository = questionRepository
        this.answerRepository = answerRepository
        this.quizRepository = quizRepository
        this.courseRepository = courseRepository
    }

    List<Quiz> list() {
        quizRepository.findAllByOrderByName()
    }

    Quiz get(final Long id) {
        quizRepository.findOne(id)
    }

    @Transactional
    void delete(final Long id) {
        Quiz quiz = quizRepository.findOne(id)
        courseRepository.findAllByQuiz(quiz)?.each {
            it.quiz = null
            courseRepository.save(it)
        }
        quizRepository.delete(quiz)
    }

    @Transactional
    Quiz saveQuiz(final Long quizId,
                  final Locale locale,
                  final Range maxQuestions,
                  final Range maxAnswers,
                  final Map<String, String> data) {
        Quiz quiz = quizId ? quizRepository.findOne(quizId) : new Quiz()
        quiz.name = data['name']
        // Get quiz
        List<Question> questions = []
        // save data
        maxQuestions.each { Integer questionIdx ->
            // get question from the map
            String textQuestion = data["question_${questionIdx}"]
            String deleteQuestion = data["delete_question_${questionIdx}"]
            Question question = quiz?.questions?.find { it.idx == questionIdx } ?: new Question(idx: questionIdx)
            // actions on question
            if (deleteQuestion) {
                if (question.id) {
                    questionRepository.delete(question)
                }
            } else if (textQuestion) {
                question.text = textService.save(question.text, locale, textQuestion)
                List<Answer> answers = []
                // go over answers
                maxAnswers.each { Integer answerIdx ->
                    // get question from the map
                    String textAnswer = data["answer_${questionIdx}_${answerIdx}"]
                    String deleteAnswer = data["delete_answer_${questionIdx}_${answerIdx}"]
                    boolean valid = data["valid_answer_${questionIdx}"] == answerIdx.toString() ?: false
                    Answer answer = question?.answers?.find { it.idx == answerIdx } ?: new Answer(idx: answerIdx)
                    answer.valid = valid
                    // actions
                    if (deleteAnswer) {
                        if (answers.id) {
                            answerRepository.delete(answer)
                        }
                    } else if (textAnswer) {
                        answer.text = textService.save(answer.text, locale, textAnswer)
                        answers << answerRepository.save(answer)
                    }
                }
                question.answers = answers
                questions << questionRepository.save(question)
            }
        }
        // save course itself
        quiz.questions = questions
        return quizRepository.save(quiz)
    }

}
