package tbt.app.services.academy

import org.springframework.stereotype.Service
import tbt.app.domains.academy.Course
import tbt.app.domains.academy.Lesson
import tbt.app.domains.tag.Tag
import tbt.app.repositories.academy.LessonRepository
import tbt.app.repositories.tag.TagRepository
import tbt.app.services.i18n.TextService

import javax.transaction.Transactional

@Service
class LessonService {

    private final TextService textService

    private final TagRepository tagRepository

    private final LessonRepository lessonRepository

    LessonService(TextService textService,
                  TagRepository tagRepository,
                  LessonRepository lessonRepository) {
        this.textService = textService
        this.tagRepository = tagRepository
        this.lessonRepository = lessonRepository
    }

    @Transactional
    List<Lesson> save(final Course course,
                      final Locale locale,
                      final Range maxLessons,
                      final Map<String, String> data) {

        List<Lesson> lessons = []

        maxLessons.each { Integer idx ->
            Lesson lesson = course?.lessons?.find { it.idx == idx } ?: new Lesson(idx: idx)
            def videoId = data["videoid_${idx}"] ?: null
            def text = data["text_${idx}"] ?: null
            def delete = data["delete_${idx}"] ?: null

            if (delete) {
                if (lesson.id) {
                    lessonRepository.delete(lesson)
                }
            } else if (videoId && text) {
                lesson.text = textService.save(lesson.text, locale, text)
                lesson.videoId = videoId
                lesson.main = data["main_${idx}"] ? true : false
                lesson.tags = getTags(data["tags_${idx}"])
                lessons << lessonRepository.save(lesson)
            }
        }

        return lessons
    }

    private List<Tag> getTags(final String ids) {
        ids?.tokenize(',')?.collect { tagRepository.findOne(it.toLong()) } ?: []
    }

}
