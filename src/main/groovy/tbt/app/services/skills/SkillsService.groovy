package tbt.app.services.skills

import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.annotation.Cacheable
import org.springframework.context.MessageSource
import org.springframework.stereotype.Service
import tbt.app.common.skill.SkillDto
import tbt.app.domains.skill.Category
import tbt.app.domains.skill.Skill
import tbt.app.repositories.academy.CourseRepository
import tbt.app.repositories.skill.SkillRepository

import javax.transaction.Transactional

@Service
class SkillsService {

    @Value('${tbt.academy.defaultSkill}')
    private String defaultSkill

    private final SkillRepository skillRepository

    private final MessageSource messageSource

    private final CourseRepository courseRepository

    SkillsService(SkillRepository skillRepository, MessageSource messageSource, CourseRepository courseRepository) {
        this.skillRepository = skillRepository
        this.messageSource = messageSource
        this.courseRepository = courseRepository
    }

    @Cacheable("skills")
    @Transactional
    Map<Category, List<SkillDto>> getSkills() {
        skillRepository
                .findAll()
                .collect { [skill: it, i18n: translate(it)] }
                .sort { it.i18n }
                .collect { new SkillDto(it.skill, hasAcademyTest(it.skill)) }
                .groupBy { it.category }
                .sort { it.key }
    }

    private boolean hasAcademyTest(final Skill skill) {
        courseRepository.findBySkill(skill).test != null
    }

    private String translate(final Skill skill) {
        messageSource.getMessage("skill.${skill.code}".toLowerCase(), null, Locale.FRENCH)
    }

    Skill getSkillOrDefault(final Long id = null) {
        id ?
                skillRepository.findOne(id) ?: skillRepository.findByCode(defaultSkill) :
                skillRepository.findByCode(defaultSkill)
    }

    Skill getSkill(final Long id) {
        id ? skillRepository.findOne(id) : null
    }

    Skill getSkill(final String code) {
        code ? skillRepository.findByCode(code) : null
    }

}
