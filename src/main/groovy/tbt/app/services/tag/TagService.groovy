package tbt.app.services.tag

import org.springframework.stereotype.Service
import tbt.app.domains.tag.Tag
import tbt.app.repositories.academy.LessonRepository
import tbt.app.repositories.tag.TagRepository

import javax.transaction.Transactional

@Service
class TagService {

    private final TagRepository tagRepository

    private final LessonRepository lessonRepository

    TagService(TagRepository tagRepository,
               LessonRepository lessonRepository) {
        this.tagRepository = tagRepository
        this.lessonRepository = lessonRepository
    }

    List<Tag> list() {
        tagRepository.findAllByOrderByName()
    }

    @Transactional
    Tag save(final Long id, final String name, final Locale locale) {
        Tag tag = id ? tagRepository.findOne(id) : new Tag(locale: locale)
        tag.name = name
        tagRepository.save(tag)
    }

    @Transactional
    Tag delete(final Long id) {
        Tag tag = tagRepository.findOne(id)
        lessonRepository.findAllByTags_Id(id)?.each {
            it.tags.remove(tag)
            lessonRepository.save(tag)
        }
        tagRepository.delete(tag)
    }
}
