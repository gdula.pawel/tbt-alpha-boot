package tbt.app.interceptors

import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import tbt.app.services.security.SecurityService

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class LoggedInUserDataInterceptor extends HandlerInterceptorAdapter implements Interceptor {

    private final SecurityService securityService

    LoggedInUserDataInterceptor(SecurityService securityService) {
        this.securityService = securityService
    }

    @Override
    void postHandle(HttpServletRequest request,
                    HttpServletResponse response,
                    Object handler,
                    ModelAndView modelAndView) throws Exception {

        if (modelAndView) {
            modelAndView.addObject("loggedInUserId", securityService.getLoggedInUserId())
        }

        super.postHandle(request, response, handler, modelAndView)
    }
}
