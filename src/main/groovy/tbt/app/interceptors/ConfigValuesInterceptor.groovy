package tbt.app.interceptors

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
@Component
class ConfigValuesInterceptor extends HandlerInterceptorAdapter implements Interceptor {

    @Value('${tbt.tracking.google.enabled}')
    private String trackingGoogleEnabled

    @Value('${tbt.tracking.google.code}')
    private String trackingGoogleCode

    @Override
    void postHandle(HttpServletRequest request,
                    HttpServletResponse response,
                    Object handler,
                    ModelAndView modelAndView) throws Exception {

        if (modelAndView) {
            modelAndView.addObject('config', [
                    'tracking.google.enabled': trackingGoogleEnabled,
                    'tracking.google.code'   : trackingGoogleCode
            ])
        }

        super.postHandle(request, response, handler, modelAndView)
    }


}
