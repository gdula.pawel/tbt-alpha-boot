package tbt.app.interceptors

import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import tbt.app.services.skills.SkillsService

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class MainMenuDataInterceptor extends HandlerInterceptorAdapter implements Interceptor {

    private final SkillsService skillsService

    MainMenuDataInterceptor(SkillsService skillsService) {
        this.skillsService = skillsService
    }

    @Override
    void postHandle(HttpServletRequest request,
                    HttpServletResponse response,
                    Object handler,
                    ModelAndView modelAndView) throws Exception {

        if (modelAndView) {
            modelAndView.addObject("mainMenuSkills", skillsService.getSkills())
        }

        super.postHandle(request, response, handler, modelAndView)
    }
}
