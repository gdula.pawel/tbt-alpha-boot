package tbt.app.interceptors

import org.springframework.web.servlet.AsyncHandlerInterceptor

/**
 * Marker interface. {@see AppConfig}
 */
interface Interceptor extends AsyncHandlerInterceptor {
}
