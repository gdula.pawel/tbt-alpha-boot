package tbt.app.interceptors

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import tbt.app.common.util.ReferralUtils
import tbt.app.services.security.SecurityService

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j
@Component
class ReferralInterceptor extends HandlerInterceptorAdapter implements Interceptor {

    private final SecurityService securityService

    ReferralInterceptor(SecurityService securityService) {
        this.securityService = securityService
    }

    @Override
    void postHandle(HttpServletRequest request,
                    HttpServletResponse response,
                    Object handler,
                    ModelAndView modelAndView) throws Exception {

        if (ReferralUtils.isSet(request)) {
            ReferralUtils.store(request)
        }

        super.postHandle(request, response, handler, modelAndView)
    }


}
