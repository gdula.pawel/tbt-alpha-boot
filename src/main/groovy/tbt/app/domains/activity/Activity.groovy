package tbt.app.domains.activity

import org.hibernate.annotations.CreationTimestamp
import tbt.app.domains.activity.content.ActivityContent

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(indexes = [
        @Index(columnList = "userId,type", name = "activity_user_id_type_idx")
])
class Activity {

    @Transient
    @Lazy
    private ActivityContent activityContent = {
        type.decode(version, body)
    }()

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public ActivityType type

    @Column(nullable = false)
    public Long userId

    @NotNull
    public Integer version

    @CreationTimestamp
    public Date created

    // TODO change to JSONB
    @Column(columnDefinition = "TEXT")
    public String body

    /**
     * Utility method to cast to specific type
     */
    @Transient
    <T extends ActivityContent> T content(Class<T> clazz) {
        if (!type.supports(version, clazz)) {
            throw new IllegalStateException("This activity doesn't support conent ${clazz}")
        }

        (T) activityContent
    }

}
