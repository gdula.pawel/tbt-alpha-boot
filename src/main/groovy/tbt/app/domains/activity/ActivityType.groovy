package tbt.app.domains.activity

import com.google.gson.Gson
import tbt.app.domains.activity.content.*

enum ActivityType {

    /**
     * Academy related activity
     */
    ACADEMY_QUIZ_SUBMITTED([1: AcademyQuizSubmittedV1]),
    ACADEMY_LESSON_VIDEO_STARTED([1: AcademyVideoStartedV1]),
    ACADEMY_LESSON_VIDEO_PAUSED([1: AcademyVideoPausedV1]),
    ACADEMY_LESSON_VIDEO_SEEN([1: AcademyVideoSeenV1]),

    /**
     * Challenges related activity
     */
    CHALLENGE_VIDEO_UPLOADED([1: ChallengeVideoUploadedV1]),
    CHALLENGE_VIDEO_SCORED([1: ChallengeVideoScoredV1]),

    /**
     * Account
     */
    ACCOUNT_CREATED([1: AccountCreatedV1]),

    /**
     * Profile related activity
     */

    PROFILE_INFO_UPDATED([1: ProfileInfoUpdatedV1]),
    PROFILE_RATING_UPDATED([1: ProfileRatingUpdatedV1]),

    /**
     * Score
     */
    SCORE_CHANGED([1: ScoreChangedV1]),

    /**
     * Referral
     */
    REFERRAL_USER_REGISTERED([1: ReferralUserRegisteredV1]),

    /**
     * Enum definition
     */
    private static final Gson GSON = new Gson()

    private final Map<Integer, Class<? extends ActivityContent>> mapping

    ActivityType(Map<Integer, Class<? extends ActivityContent>> mapping) {
        this.mapping = mapping
    }

    ActivityContent decode(final Integer version, final String body) {
        GSON.fromJson(body, mapping[version])
    }

    String encode(final ActivityContent content) {
        def data = content.properties
        data.remove('class')
        data.remove('type')
        data.remove('version')
        GSON.toJson(data)
    }

    boolean supports(final Integer version, final Class<? extends ActivityContent> clazz) {
        mapping[version] == clazz
    }

}
