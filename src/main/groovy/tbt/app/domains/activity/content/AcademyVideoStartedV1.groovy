package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType

class AcademyVideoStartedV1 implements ActivityContent {

    Long skillId

    Long courseId

    Long lessonId

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.ACADEMY_LESSON_VIDEO_STARTED
    }
}
