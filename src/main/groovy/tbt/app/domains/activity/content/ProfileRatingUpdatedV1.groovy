package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType

class ProfileRatingUpdatedV1 implements ActivityContent {

    List<Change> changes

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.PROFILE_RATING_UPDATED
    }

    static class Change {

        Long skillId

        Integer oldValue

        Integer newValue
    }
}
