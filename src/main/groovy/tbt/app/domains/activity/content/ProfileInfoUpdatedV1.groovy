package tbt.app.domains.activity.content

import groovy.transform.ToString
import tbt.app.domains.activity.ActivityType

@ToString
class ProfileInfoUpdatedV1 implements ActivityContent {

    List<Change> changes

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.PROFILE_INFO_UPDATED
    }

    @ToString
    static class Change {

        String name

        String oldValue

        String newValue

    }
}
