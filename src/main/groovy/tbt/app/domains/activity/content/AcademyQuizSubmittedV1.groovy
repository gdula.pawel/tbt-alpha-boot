package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType

class AcademyQuizSubmittedV1 implements ActivityContent {

    Long courseId

    Long skillId

    Float score

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.ACADEMY_QUIZ_SUBMITTED
    }
}
