package tbt.app.domains.activity.content

import groovy.transform.ToString
import tbt.app.domains.activity.ActivityType

@ToString
class ReferralUserRegisteredV1 implements ActivityContent {

    Long registeredUserId

    Long referringUserId

    Source source

    @Override
    ActivityType type() {
        return ActivityType.REFERRAL_USER_REGISTERED
    }

    @Override
    Integer version() {
        return 1
    }


    enum Source {
        FACEBOOK
    }
}
