package tbt.app.domains.activity.content

import groovy.transform.ToString
import tbt.app.domains.activity.ActivityType

@ToString
class AcademyVideoSeenV1 implements ActivityContent {

    Long skillId

    Long courseId

    Long lessonId

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.ACADEMY_LESSON_VIDEO_SEEN
    }
}
