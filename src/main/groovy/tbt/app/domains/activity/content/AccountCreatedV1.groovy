package tbt.app.domains.activity.content

import groovy.transform.ToString
import tbt.app.domains.activity.ActivityType

@ToString
class AccountCreatedV1 implements ActivityContent {

    Long userId

    Source source

    @Override
    ActivityType type() {
        return ActivityType.ACCOUNT_CREATED
    }

    @Override
    Integer version() {
        return 1
    }


    enum Source {
        FACEBOOK
    }
}
