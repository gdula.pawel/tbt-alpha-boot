package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType
import tbt.app.domains.challenge.ChallengeVideoContext

class ChallengeVideoUploadedV1 implements ActivityContent {

    ChallengeVideoContext context

    Long videoId

    Long courseId

    Long skillId

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.CHALLENGE_VIDEO_UPLOADED
    }
}
