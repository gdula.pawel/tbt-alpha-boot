package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType

interface ActivityContent {

    Integer version()

    ActivityType type()

}