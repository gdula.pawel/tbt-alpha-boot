package tbt.app.domains.activity.content

import tbt.app.domains.activity.ActivityType

class ScoreChangedV1 implements ActivityContent {

    ActivityType trigger

    Long triggerId

    Integer points

    Integer delta

    @Override
    Integer version() {
        return 1
    }

    @Override
    ActivityType type() {
        return ActivityType.SCORE_CHANGED
    }
}
