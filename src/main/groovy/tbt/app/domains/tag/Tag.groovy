package tbt.app.domains.tag

import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.*

@Entity
@Table(name = "tag", indexes = [
        @Index(columnList = "name,locale", name = "tag_name_locale_idx", unique = true)
])
class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @NotEmpty
    @NotBlank
    @Column(nullable = false)
    public String name

    @Column(nullable = false)
    Locale locale

    @PreUpdate
    void cleanUpName() {
        if (this.name != null) {
            this.name = this.name.toLowerCase()
        }
    }


}
