package tbt.app.domains.challenge

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotEmpty
import tbt.app.domains.academy.Course
import tbt.app.domains.user.User

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(indexes = [
        @Index(columnList = "user_id,context", name = "challenge_video_user_context_idx"),
        @Index(columnList = "user_id,state", name = "challenge_video_state_idx"),
        @Index(columnList = "state", name = "challenge_state_idx"),
])
class ChallengeVideo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @NotNull
    @NotEmpty
    public String name

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public ChallengeVideoState state

    @Column(nullable = true)
    public String poster

    @OneToOne
    public User user

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public ChallengeVideoContext context

    @OneToOne
    public Course course

    @Column(nullable = true)
    public Integer score

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated

    @Column(nullable = true)
    public Date confirmed
}
