package tbt.app.domains.challenge

enum ChallengeVideoState {

    UPLOAD_INITIALIZED(1),
    UPLOAD_CONFIRMED(2)

    final Integer order

    ChallengeVideoState(Integer order) {
        this.order = order
    }
}
