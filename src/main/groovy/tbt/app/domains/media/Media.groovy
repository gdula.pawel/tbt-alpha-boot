package tbt.app.domains.media

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.social.facebook.api.User

import javax.persistence.*
import javax.validation.constraints.NotNull

class Media {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @NotNull
    public String path

    @OneToOne
    public User user

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public MediaContext type

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated

}
