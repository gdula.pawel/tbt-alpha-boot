package tbt.app.domains.file

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotNull

class File {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @NotNull
    @NotEmpty
    public String name

    @NotNull
    public Long userId

    @CreationTimestamp
    public Date created
}
