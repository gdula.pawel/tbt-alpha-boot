package tbt.app.domains.i18n

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob

@Entity
class Text {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

    @NotEmpty
    @NotBlank
    @Column(columnDefinition = "TEXT")
    String content

    @Column(nullable = false)
    Locale locale

}
