package tbt.app.domains.user

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.Email
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Transient

@Entity
class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Email
    @NotEmpty
    @NotBlank
    String email

    String avatar

    String firstName

    String lastName

    String gender

    Integer ageRangeMin

    Integer ageRangeMax

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

    @Transient
    String getFullName() {
        "$firstName $lastName"
    }

}
