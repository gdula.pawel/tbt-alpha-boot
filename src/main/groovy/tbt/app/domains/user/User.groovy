package tbt.app.domains.user

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp

import javax.persistence.*

@Entity
@Table(name = "app_user", indexes = [
        @Index(columnList = "externalId", name = "principal_external_id_idx")
])
class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    public String externalId

    public Long referringUserId

    @OneToOne
    public Profile profile

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated

}
