package tbt.app.domains.user;

enum Role {

    USER,
    ADMIN

    String asAuthority() {
        name()
    }
}
