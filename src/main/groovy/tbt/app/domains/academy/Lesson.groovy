package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty
import tbt.app.domains.i18n.Text
import tbt.app.domains.tag.Tag

import javax.persistence.*

@Entity
class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Column(nullable = false)
    Integer idx

    @OneToMany(cascade = CascadeType.REMOVE)
    List<Text> text

    @Column(nullable = false)
    @NotBlank
    @NotEmpty
    String videoId

    boolean main = false

    @ManyToMany
    List<Tag> tags

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated
}
