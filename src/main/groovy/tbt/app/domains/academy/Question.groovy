package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import tbt.app.domains.i18n.Text

import javax.persistence.*

@Entity
class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Column(nullable = false)
    Integer idx

    @OneToMany(cascade = CascadeType.REMOVE)
    List<Text> text

    @OneToMany(cascade = CascadeType.REMOVE)
    List<Answer> answers

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

}
