package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.*

@Entity
class Quiz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

    @Column(nullable = false)
    @NotBlank
    @NotEmpty
    String name

    @OneToMany
    List<Question> questions

}
