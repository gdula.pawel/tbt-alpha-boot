package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import tbt.app.domains.skill.Skill

import javax.persistence.*

@Entity
class QuizResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Column(nullable = false)
    Long userId

    @ManyToOne
    Skill skill

    Integer result

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

}
