package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import tbt.app.domains.skill.Skill

import javax.persistence.*

@Entity
class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @OneToOne
    Skill skill

    @OneToOne
    Test test

    @OneToOne
    Quiz quiz

    @OneToMany
    List<Lesson> lessons

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

    List<Lesson> getMainLessons() {
        lessons.findAll { it.main }
    }

    List<Lesson> getComplementaryLessons() {
        lessons.findAll { !it.main }
    }

}
