package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty
import tbt.app.domains.i18n.Text

import javax.persistence.*

@Entity
class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

    @OneToMany
    List<Text> requirements

    @OneToMany
    List<Text> testExplanation

    @OneToMany
    List<Text> videoExplanation

    @OneToMany
    List<Text> scoreCalculation

    @OneToMany
    List<Text> videoTitle

    @NotBlank
    @NotEmpty
    @Column(nullable = false)
    String explanationVideoId

    @Column(nullable = false)
    Integer explanationImageId

    @Column(nullable = false)
    Integer expectedTime
}
