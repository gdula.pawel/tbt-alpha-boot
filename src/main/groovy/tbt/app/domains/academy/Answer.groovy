package tbt.app.domains.academy

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotEmpty
import tbt.app.domains.i18n.Text

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.validation.constraints.NotNull

@Entity
class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id

    @Column(nullable = false)
    Integer idx

    @OneToMany(cascade = CascadeType.REMOVE)
    List<Text> text

    boolean valid = false

    @CreationTimestamp
    Date created

    @UpdateTimestamp
    Date updated

}
