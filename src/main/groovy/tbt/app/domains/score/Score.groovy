package tbt.app.domains.score

import tbt.app.domains.activity.ActivityType

class Score {

    static final Integer MAX_SCORE = 10

    static final Integer MAX_POINTS = 16129

    final Integer points

    final Date date

    final ActivityType baseOn

    Score(Integer points, Date date, ActivityType baseOn) {
        this.points = points
        this.date = date
        this.baseOn = baseOn
    }

    Score(Integer points, Date forDay) {
        this(points, forDay, null)
    }

    Score(Integer points) {
        this(points, new Date().clearTime())
    }

    Double getScore() {
        Math.round(((points / MAX_POINTS) * MAX_SCORE) * 10) / 10
    }

    Integer getMaxScore() {
        MAX_SCORE
    }

    Integer getPercentage() {
        getScore() * 10
    }
}
