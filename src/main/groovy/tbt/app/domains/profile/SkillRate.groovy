package tbt.app.domains.profile

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import tbt.app.domains.skill.Skill

import javax.persistence.*

@Entity
class SkillRate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public RateType type

    @Column(nullable = false)
    public Long userId

    @OneToOne
    public Skill skill

    @Column(nullable = false)
    public Integer rate

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated

}
