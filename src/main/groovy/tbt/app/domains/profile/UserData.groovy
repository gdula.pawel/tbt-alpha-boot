package tbt.app.domains.profile

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp

import javax.persistence.*
import java.beans.Transient

/**
 * Created by KKACHEL on 17.12.2017.
 */
@Entity
class UserData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @Column(nullable = false)
    public Long userId

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public PositionType position

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public BestLegType bestLeg

    public Integer growth

    public Integer weight

    @Temporal(TemporalType.DATE)
    public Date dateOfBirth

    public String teamName

    public String teamLeague

    public Integer playerNumber

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated

    @Transient
    Integer getAge() {
        dateOfBirth ? (new Date().year - dateOfBirth.year) : null
    }
}
