package tbt.app.domains.profile

/**
 * Created by KKACHEL on 17.12.2017.
 */
enum PositionType {

    GOALKEEPER("profile.userData.enum.position.goalkeeper"),

    CENTER_BACK("profile.userData.enum.position.defender.centerBack"),

    LEFT_BACK("profile.userData.enum.position.defender.leftBack"),

    RIGHT_BACK("profile.userData.enum.position.defender.rightBack"),

    DEFENSIVE_MIDFIELDER("profile.userData.enum.position.midfielder.defensiveMidfielder"),

    CENTRAL_MIDFIELDER("profile.userData.enum.position.midfielder.centralMidfielder"),

    ATTACKING_MIDFIELDER("profile.userData.enum.position.midfielder.attackingMidfielder"),

    RIGHT_WING("profile.userData.enum.position.forward.rightWing"),

    LEFT_WING("profile.userData.enum.position.forward.leftWing"),

    CENTER_FORWARD("profile.userData.enum.position.forward.centerForward")

    String i18nCode

    PositionType(String i18nCode) {
        this.i18nCode = i18nCode
    }

}
