package tbt.app.domains.profile

/**
 * Created by KKACHEL on 17.12.2017.
 */
enum BestLegType {

    RIGHT("profile.userData.enum.bestLeg.right"),

    LEFT("profile.userData.enum.bestLeg.left"),

    BOTH("profile.userData.enum.bestLeg.both")

    String i18nCode

    BestLegType(String i18nCode) {
        this.i18nCode = i18nCode
    }
}
