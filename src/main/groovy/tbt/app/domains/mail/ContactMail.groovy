package tbt.app.domains.mail

class ContactMail {

    final String from

    final String body

    ContactMail(String from, String body) {
        this.from = from
        this.body = body
    }
}
