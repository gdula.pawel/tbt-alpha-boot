package tbt.app.domains.mail

import tbt.app.domains.user.Profile

class RegistrationMail {

    final Profile profile

    final Locale locale

    RegistrationMail(Profile profile, Locale locale) {
        this.profile = profile
        this.locale = locale
    }
}
