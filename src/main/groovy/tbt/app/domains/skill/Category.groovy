package tbt.app.domains.skill;

enum Category {

    PHYSICAL(1,),
    TECHNICAL(2),
    FREESTYLE(3,),
    MENTAL(4),
    META(5)

    final Integer priority

    Category(Integer priority) {
        this.priority = priority
    }

    @Lazy
    static List<Category> sortedValues = {
        values().sort { it.priority }
    }()
}
