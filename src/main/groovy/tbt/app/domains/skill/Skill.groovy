package tbt.app.domains.skill;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*

@Entity
class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @NotEmpty
    @NotBlank
    @Column(nullable = false)
    public String code

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    public Category category

    @Column(nullable = true)
    public String categoryGroup

}
