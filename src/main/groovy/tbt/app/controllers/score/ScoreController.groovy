package tbt.app.controllers.score

import groovy.util.logging.Slf4j
import org.springframework.metrics.annotation.Timed
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import tbt.app.domains.score.Score
import tbt.app.services.score.ScoreService
import tbt.app.services.security.SecurityService

@Slf4j
@RestController
@Timed
class ScoreController {

    private final ScoreService scoreService

    private final SecurityService securityService

    ScoreController(ScoreService scoreService, SecurityService securityService) {
        this.scoreService = scoreService
        this.securityService = securityService
    }

    @RequestMapping(path = "/score", method = RequestMethod.GET)
    def greeting() {
        [
                current: scoreService.get(securityService.loggedInUser.id).score,
                max    : Score.MAX_SCORE
        ]
    }
}
