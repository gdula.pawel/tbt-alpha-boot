package tbt.app.controllers.staticpage

import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@Timed
class StaticPageController {

    @RequestMapping("/faq")
    String faq() {
        return "static/faq"
    }

    @RequestMapping("/termsandconditions")
    String imprint() {
        return "static/termsandconditions"
    }

    @RequestMapping("/invite-friend")
    String inviteFriend() {
        return "static/inviteFriend"
    }

}
