package tbt.app.controllers.profile

import groovy.util.logging.Slf4j
import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.util.WebUtils
import tbt.app.domains.profile.RateType
import tbt.app.domains.profile.UserData
import tbt.app.domains.user.User
import tbt.app.services.academy.QuizResultService
import tbt.app.services.challenge.ChallengeVideoService
import tbt.app.services.profile.SkillRateService
import tbt.app.services.profile.UserDataService
import tbt.app.services.score.ScoreHistoryService
import tbt.app.services.score.ScoreService
import tbt.app.services.security.SecurityService
import tbt.app.services.skills.SkillsService

@Slf4j
@Controller
@Timed
class ProfileController {

    private final SkillsService skillsService

    private final SkillRateService skillRateService

    private final SecurityService securityService

    private final UserDataService userDataService

    private final FileManagerFactory fileManagerFactory

    private final QuizResultService quizResultService

    private final ChallengeVideoService challengeService

    private final ScoreService scoreService

    private final ScoreHistoryService scoreHistoryService

    ProfileController(SkillsService skillsService,
                      SkillRateService skillRateService,
                      SecurityService securityService,
                      UserDataService userDataService,
                      FileManagerFactory fileManagerFactory,
                      QuizResultService quizResultService,
                      ChallengeVideoService challengeService,
                      ScoreService scoreService,
                      ScoreHistoryService scoreHistoryService) {
        this.skillsService = skillsService
        this.skillRateService = skillRateService
        this.securityService = securityService
        this.userDataService = userDataService
        this.fileManagerFactory = fileManagerFactory
        this.quizResultService = quizResultService
        this.challengeService = challengeService
        this.scoreService = scoreService
        this.scoreHistoryService = scoreHistoryService
    }

    @RequestMapping("/profile")
    String profile(final Model model) {
        User loggedUser = securityService.getLoggedInUser()
        UserData userData = userDataService.getUserData()
        Map<RateType, List<String, Integer>> skillRates = skillRateService.getGroupedSkillsByTypeForCurrentUser()
        Map<Long, Integer> quizResults = quizResultService.getGroupedQuizResultsBySkillIdForCurrentUser()
        model['userProfile'] = loggedUser.profile
        model['userData'] = userData
        model['skillUserRates'] = skillRates.get(RateType.USER_RATE)
        model['skillTbtRates'] = skillRates.get(RateType.TBT_RATE)
        model['skills'] = skillsService.getSkills()
        model['quizResults'] = quizResults
        model['avatar'] = fileManagerFactory.get().getPath(loggedUser.profile.avatar, FileType.AVATAR)
        model['score'] = scoreService.get(loggedUser.id)
        return "profile/profile"
    }

    @RequestMapping("/profile/score")
    String score(final Model model) {
        User loggedUser = securityService.getLoggedInUser()
        model['userProfile'] = loggedUser.profile
        model['avatar'] = fileManagerFactory.get().getPath(loggedUser.profile.avatar, FileType.AVATAR)
        model['score'] = scoreService.get(loggedUser.id)
        model['scoreAggregation'] = scoreHistoryService.getPerDay(loggedUser.id)
        model['scoreDetails'] = scoreHistoryService.getDetailed(loggedUser.id)
        return "profile/score"
    }

    @RequestMapping(path = "/profile/{category}/save", method = RequestMethod.POST)
    String saveSkillRates(@PathVariable final String category,
                          @RequestParam final Map<String, String> data,
                          final RedirectAttributes redirAttrs) {
        safeSave(redirAttrs) {
            Map<String, Integer> convertedData = data.collectEntries { [(it.key): it.value.toInteger()] }
            skillRateService.saveUserSkillRate(convertedData)
        }
    }

    @RequestMapping("/profile/userData/save")
    String saveUserData(@RequestParam final Map data,
                        final RedirectAttributes redirAttrs) {
        safeSave(redirAttrs) {
            userDataService.saveUserData(data)
        }
    }

    @RequestMapping("/profile/mediagallery")
    String showMediaGallery(final Model model) {
        model['videos'] = challengeService.getAllCurrentUserVideos()
        return "profile/mediaGallery"
    }

    private String safeSave(final RedirectAttributes redirAttrs, final Closure worker) {
        try {
            worker.call()
            redirAttrs.addFlashAttribute('status', 'success')
            redirAttrs.addFlashAttribute('message', 'profile.save.success')
            return WebUtils.redirectTo('/profile')
        } catch (Exception exp) {
            log.error('There was an exception while saving user profile', exp)
            redirAttrs.addFlashAttribute('status', 'error')
            redirAttrs.addFlashAttribute('message', 'profile.save.error')
            return WebUtils.redirectTo('/profile')
        }
    }

}