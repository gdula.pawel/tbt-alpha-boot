package tbt.app.controllers.contact

import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.util.WebUtils
import tbt.app.domains.mail.ContactMail
import tbt.app.services.mail.MailService
import tbt.app.services.security.SecurityService

@Controller
@Timed
class ContactController {

    private final SecurityService securityService

    private final MailService service

    ContactController(SecurityService securityService,
                      MailService service) {
        this.securityService = securityService
        this.service = service
    }

    @RequestMapping("/contact")
    String index(final Model model) {
        model['user'] = securityService.getLoggedInUser()
        return "contact/contact"
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    String send(@RequestParam final Map data,
                final Model model,
                final RedirectAttributes redirAttrs) {
        if (!data.email || !data.body) {
            model['email'] = data.email
            model['body'] = data.body
            model['status'] = 'error'
            return index(model)
        }

        service.send(new ContactMail(data.email, data.body))
        redirAttrs.addFlashAttribute('status', 'success')
        return WebUtils.redirectTo('/contact')
    }

}
