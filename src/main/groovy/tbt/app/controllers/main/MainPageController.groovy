package tbt.app.controllers.main

import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@Timed
class MainPageController {

    @RequestMapping("/")
    String main() {
        return "mainPage/main"
    }


}
