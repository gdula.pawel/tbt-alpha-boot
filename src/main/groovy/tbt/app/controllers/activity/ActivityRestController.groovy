package tbt.app.controllers.activity

import org.springframework.metrics.annotation.Timed
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import tbt.app.domains.activity.content.AcademyVideoPausedV1
import tbt.app.domains.activity.content.AcademyVideoSeenV1
import tbt.app.domains.activity.content.AcademyVideoStartedV1
import tbt.app.domains.activity.content.ActivityContent
import tbt.app.services.activity.ActivityService

@RestController
@Timed
class ActivityRestController {

    private final ActivityService activityService

    ActivityRestController(ActivityService activityService) {
        this.activityService = activityService
    }

    @RequestMapping(path = "/api/activity/academy/video", method = RequestMethod.POST)
    def saveAcademyVideo(@RequestParam final Map<String, String> data) {
        activityService.append(getContent(data))
        return "OK"
    }

    private ActivityContent getContent(final Map<String, String> data) {
        Long skillId = data.skillId.toLong()
        Long courseId = data.courseId.toLong()
        Long lessonId = data.lessonId.toLong()
        switch (data.state) {
            case 'started':
                return new AcademyVideoStartedV1(skillId: skillId, courseId: courseId, lessonId: lessonId)
            case 'seen':
                return new AcademyVideoSeenV1(skillId: skillId, courseId: courseId, lessonId: lessonId)
            case 'paused':
                return new AcademyVideoPausedV1(skillId: skillId, courseId: courseId, lessonId: lessonId)
            default:
                throw new IllegalArgumentException("State '${data.state}' not supported")
        }
    }
}
