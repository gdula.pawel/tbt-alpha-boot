package tbt.app.controllers.academy

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.metrics.annotation.Timed
import org.springframework.util.MimeType
import org.springframework.web.bind.annotation.*
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.util.FileUtils
import tbt.app.domains.challenge.ChallengeVideo
import tbt.app.services.challenge.ChallengeVideoService

@RestController
@Timed
class AcademyTestRestController {

    private final List<String> VALID_TYPES = ['video/mp4', 'video/mov', 'video/quicktime']

    private final Long MAX_VIDEO_SIZE = 500000000

    private final FileManagerFactory fileManagerFactory

    private final ChallengeVideoService challengeVideoService

    AcademyTestRestController(FileManagerFactory fileManagerFactory,
                              ChallengeVideoService challengeVideoService) {
        this.fileManagerFactory = fileManagerFactory
        this.challengeVideoService = challengeVideoService
    }

    @RequestMapping(path = "/api/academy/video", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity createUploadLink(@RequestParam final Map<String, String> data) {
        final Long courseId = data.courseId.toLong()
        final Long size = data.size.toLong()
        final MimeType type = MimeType.valueOf(data.type)

        if (size > MAX_VIDEO_SIZE) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body([error: 'tbt.academy.challenge.toBig'])
        }

        if (!(type.toString() in VALID_TYPES)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body([error: 'tbt.academy.challenge.invalid'])
        }

        final String fileName = FileUtils.generateName(courseId, type)
        final ChallengeVideo challengeVideo = challengeVideoService.init(courseId, fileName)
        return ResponseEntity.status(HttpStatus.OK).body([
                id : challengeVideo.id,
                url: fileManagerFactory.get().getUploadPath(fileName, type, FileType.VIDEO)
        ])

    }

    @RequestMapping(path = "/api/academy/video/{id}", method = RequestMethod.PUT)
    @ResponseBody
    Map<String, ?> confirmUpload(@PathVariable final Long id) {
        challengeVideoService.confirm(id)
        [status: "OK"]
    }

}


