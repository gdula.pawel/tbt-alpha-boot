package tbt.app.controllers.academy

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import tbt.app.common.util.WebUtils
import tbt.app.services.academy.CourseService
import tbt.app.services.skills.SkillsService

@Slf4j
@Controller
@Timed
class AcademyCoursesController {

    private final SkillsService skillsService

    private final CourseService courseService

    @Autowired
    AcademyCoursesController(SkillsService skillsService,
                             CourseService courseService) {
        this.skillsService = skillsService
        this.courseService = courseService
    }

    @RequestMapping("/academy")
    String academy() {
        WebUtils.redirectTo("/academy/course/{}/test", skillsService.getSkillOrDefault().id)
    }

    @RequestMapping("/academy/course")
    String course() {
        WebUtils.redirectTo("/academy/course/{}/test", skillsService.getSkillOrDefault().id)
    }


    @RequestMapping("/academy/course/{skillId}")
    String course(@PathVariable final Long skillId, final Model model) {
        model['skills'] = skillsService.getSkills()
        model['course'] = courseService.getCourseOrDefault(skillId)
        return "academy/course"
    }

}
