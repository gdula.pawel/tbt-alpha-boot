package tbt.app.controllers.academy

import groovy.util.logging.Slf4j
import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import tbt.app.common.util.WebUtils
import tbt.app.domains.academy.Course
import tbt.app.services.academy.CourseService
import tbt.app.services.skills.SkillsService

@Slf4j
@Controller
@Timed
class AcademyTestController {

    private final SkillsService skillsService

    private final CourseService courseService

    AcademyTestController(SkillsService skillsService, CourseService courseService) {
        this.skillsService = skillsService
        this.courseService = courseService
    }

    @RequestMapping("/academy/course/{skillId}/test")
    String show(@PathVariable final Long skillId, final Model model) {
        executeWhenValid(skillId) { Course course ->
            model['course'] = course
            model['skills'] = skillsService.getSkills()
            return "academy/test"
        }
    }

    private def executeWhenValid(final Long skillId, final Closure executor) {
        final def course = courseService.getCourse(skillId)

        if (!course) {
            return WebUtils.redirectTo("/academy/course")
        }

        if (!course.test) {
            return WebUtils.redirectTo("/academy/course/{}", course.skill.id)
        }

        executor(course)
    }

}
