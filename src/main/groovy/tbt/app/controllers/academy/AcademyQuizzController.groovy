package tbt.app.controllers.academy

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.metrics.annotation.Timed
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.academy.CourseTestResultDto
import tbt.app.common.util.WebUtils
import tbt.app.domains.academy.Course
import tbt.app.services.academy.CourseService
import tbt.app.services.academy.QuizResultService
import tbt.app.services.skills.SkillsService

@Slf4j
@Controller
@Timed
class AcademyQuizzController {

    private final SkillsService skillsService

    private final CourseService courseService

    private final QuizResultService quizResultService

    @Autowired
    AcademyQuizzController(SkillsService skillsService,
                           CourseService courseService,
                           QuizResultService quizResultService) {
        this.skillsService = skillsService
        this.courseService = courseService
        this.quizResultService = quizResultService
    }

    @RequestMapping(path = "/academy/course/{skillId}/quizz", method = RequestMethod.GET)
    String show(@PathVariable final Long skillId,
                final Model model) {

        executeWhenValid(skillId) { Course course ->
            model['skills'] = skillsService.getSkills()
            model['course'] = course
            return "academy/quizz"
        }
    }

    @RequestMapping(path = "/academy/course/{skillId}/quizz", method = RequestMethod.POST)
    String save(@PathVariable final Long skillId,
                @RequestParam final Map data,
                final Model model,
                final RedirectAttributes redirAttrs) {

        executeWhenValid(skillId) { Course course ->
            def correctedData = convertData(data)
            log.info('For skill {} got answers {}', skillId, correctedData)
            CourseTestResultDto testResult = courseService.checkTest(course, correctedData)

            if (testResult.isAllQuestionsSubmitted()) {
                quizResultService.saveQuizResult(course.getSkill(), testResult.score())
                redirAttrs.addFlashAttribute('status', testResult.score() == 100 ? 'success' : 'warning')
                redirAttrs.addFlashAttribute('message', 'academy.test.success')
                redirAttrs.addFlashAttribute('percent', Math.round(testResult.score()))
                return WebUtils.redirectTo("/academy/course/{}/quizz", skillId)
            } else {
                model['skills'] = skillsService.getSkills()
                model['course'] = course
                model['status'] = 'error'
                model['message'] = 'academy.test.errors.empty'
                model['answers'] = correctedData
                return "academy/quizz"
            }
        }
    }

    private def convertData(final Map<String, String> input) {
        input.remove('data')
        input.collectEntries { [(it.key.toLong()): it.value.toLong()] }
    }

    private String executeWhenValid(final Long skillId, final Closure executor) {
        final def course = courseService.getCourse(skillId)

        if (!course) {
            return WebUtils.redirectTo("/academy/course")
        }

        executor(course)
    }
}
