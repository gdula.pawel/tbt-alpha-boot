package tbt.cms.commons.utils

class I18nCommons {

    private static final List<Locale> SUPPORTED_LOCALE = [Locale.FRANCE]

    static List<Locale> getSupportedLocale() {
        SUPPORTED_LOCALE
    }

    static Locale getDefaultLocale() {
        SUPPORTED_LOCALE.first()
    }
}
