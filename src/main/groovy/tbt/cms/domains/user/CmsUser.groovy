package tbt.cms.domains.user

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.validator.constraints.NotBlank
import org.hibernate.validator.constraints.NotEmpty

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "cms_user")
class CmsUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id

    @NotEmpty
    @NotBlank
    public String username

    @NotEmpty
    @NotBlank
    public String password

    public Boolean enabled

    @CreationTimestamp
    public Date created

    @UpdateTimestamp
    public Date updated
}
