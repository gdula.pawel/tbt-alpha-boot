package tbt.cms.controllers.security

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import tbt.app.common.util.WebUtils
import tbt.config.SecurityConfig

@Controller
class LoginController {

    @RequestMapping("/cms")
    def cms() {
        return WebUtils.redirectTo(SecurityConfig.CmsSecurityConfig.DEFAULT_SUCCESS_URL)
    }

    @RequestMapping("/cms/login")
    def index() {
        return "cms/login"
    }

}
