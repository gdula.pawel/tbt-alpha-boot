package tbt.cms.controllers.challenge

import groovy.util.logging.Slf4j
import org.apache.commons.lang.StringUtils
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.file.FileManagerFactory
import tbt.app.common.file.FileType
import tbt.app.common.file.VideoDto
import tbt.app.common.util.WebUtils
import tbt.app.services.challenge.ChallengeVideoService
import tbt.app.services.profile.UserDataService
import tbt.cms.commons.pagination.Pager

@Controller
@Slf4j
class ChallengeController {

    private static final Integer MAX_PAGE_SIZE = 20

    private static final Integer BUTTONS_TO_SHOW = 5

    private static final Range SCORE = (0..10)

    private final FileManagerFactory fileManagerFactory

    private final ChallengeVideoService challengeService

    private final UserDataService userDataService

    ChallengeController(FileManagerFactory fileManagerFactory,
                        ChallengeVideoService challengeService,
                        UserDataService userDataService) {
        this.fileManagerFactory = fileManagerFactory
        this.challengeService = challengeService
        this.userDataService = userDataService
    }

    @RequestMapping("/cms/challenge")
    def list(@RequestParam(required = false) final Integer page, final Model model) {
        def list = challengeService.list(new PageRequest(page ?: 0, MAX_PAGE_SIZE, Sort.Direction.DESC, 'created'))
        model['list'] = list
        model['pager'] = new Pager(list.getTotalPages(), list.getNumber(), BUTTONS_TO_SHOW)
        return "cms/challenge/list"
    }

    @RequestMapping(path = "/cms/challenge/{id}", method = RequestMethod.GET)
    def show(@PathVariable final Long id, final Model model) {
        def challenge = challengeService.get(id)
        model['score'] = SCORE
        model['item'] = challenge
        model['userData'] = userDataService.getUserData(challenge.user.id)
        model['video'] = new VideoDto(fileManagerFactory.get().getPath(challenge.name, FileType.VIDEO))
        return "cms/challenge/form"
    }

    @RequestMapping(path = "/cms/challenge/{id}", method = RequestMethod.POST)
    def save(@PathVariable final Long id, @RequestParam final Map<String, String> data,
             final Model model,
             final RedirectAttributes redirAttrs) {
        if (StringUtils.isEmpty(data.score)) {
            model['status'] = 'error'
            return show(id, model)
        }

        challengeService.saveScore(id, data.score.toInteger())
        redirAttrs.addFlashAttribute('status', 'success')
        return WebUtils.redirectTo("/cms/challenge/{}", id)
    }

}
