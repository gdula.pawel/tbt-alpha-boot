package tbt.cms.controllers.tag

import groovy.util.logging.Slf4j
import org.apache.commons.lang.LocaleUtils
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.util.WebUtils
import tbt.app.services.tag.TagService
import tbt.cms.commons.utils.I18nCommons

@Controller
@Slf4j
class TagController {

    private final TagService tagService

    TagController(TagService tagService) {
        this.tagService = tagService
    }

    @RequestMapping("/cms/tags")
    def list(final Model model) {
        model['supported_locale'] = I18nCommons.supportedLocale
        model['locale'] = I18nCommons.defaultLocale
        model['tags'] = tagService.list()
        return "cms/tag/list"
    }

    @RequestMapping(path = "/cms/tags", method = RequestMethod.POST)
    def add(@RequestParam final Map<String, String> data,
            final Model model,
            final RedirectAttributes redirAttrs) {
        if (!data['name']) {
            model['status'] = 'error'
            return list(model)
        }

        def id = data['id'] ? data['id'].toLong() : null
        def locale = data['locale'] ? LocaleUtils.toLocale(data['locale']) : null
        tagService.save(id, data['name'], locale)
        redirAttrs.addFlashAttribute('status', 'success')
        return WebUtils.redirectTo("/cms/tags")
    }

    @RequestMapping(path = "/cms/tags/{tagId}/delete")
    def delete(@PathVariable final Long tagId,
               final RedirectAttributes redirAttrs) {
        tagService.delete(tagId)
        redirAttrs.addFlashAttribute('status', 'success')
        return WebUtils.redirectTo("/cms/tags")
    }

}
