package tbt.cms.controllers.academy

import groovy.util.logging.Slf4j
import org.apache.commons.lang.LocaleUtils
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.util.WebUtils
import tbt.app.services.academy.CourseService
import tbt.app.services.academy.QuizService
import tbt.app.services.skills.SkillsService
import tbt.cms.commons.utils.I18nCommons

@Controller
@Slf4j
class AcademyController {

    public static final Range MAX_ALLOWED_QUESTIONS = (1..25)

    public static final Range MAX_ALLOWED_ANSWERS = (1..5)

    private static final Range SUPPORTED_TEST_IMAGE_RANGE = (1..25)

    private final SkillsService skillsService

    private final CourseService courseService

    private final QuizService quizService

    AcademyController(SkillsService skillsService,
                      CourseService courseService,
                      QuizService quizService) {
        this.skillsService = skillsService
        this.courseService = courseService
        this.quizService = quizService
    }

    @RequestMapping("/cms/academy")
    def index(final Model model) {
        model['skills'] = skillsService.getSkills()
        return "cms/academy/list"
    }

    /*
        --------------------------------------
        Test page content admin
        --------------------------------------
     */

    @RequestMapping(path = "/cms/academy/{skillId}/test", method = RequestMethod.GET)
    String testGet(@PathVariable final Long skillId, final Model model) {
        model['skill'] = skillsService.getSkill(skillId)
        model['test'] = courseService.getCourse(skillId)?.test
        model['supported_test_image'] = SUPPORTED_TEST_IMAGE_RANGE
        model['supported_locale'] = I18nCommons.supportedLocale
        model['locale'] = I18nCommons.defaultLocale
        return "cms/academy/test/form"
    }

    @RequestMapping(path = "/cms/academy/{skillId}/test", method = RequestMethod.POST)
    String testSave(@PathVariable final Long skillId,
                    @RequestParam final Map<String, String> data,
                    final Model model,
                    final RedirectAttributes redirAttrs) {
        def locale = LocaleUtils.toLocale(data['locale'])

        try {
            courseService.saveTest(skillId, locale, data)
            redirAttrs.addFlashAttribute('status', 'success')
            return WebUtils.redirectTo("/cms/academy/{}/test", skillId)
        } catch (Exception exp) {
            // we need to setup a key for map, in other case we have an exception
            // radio button doesn't set up value when not selected
            data.explanationImageId = data['explanationImageId'] ?: null
            data.videoTitle = data['videoTitle'] ?: null
            // build model
            log.warn('There was an exception while saving ', exp)
            model['status'] = 'error'
            model['supported_test_image'] = SUPPORTED_TEST_IMAGE_RANGE
            model['supported_locale'] = I18nCommons.supportedLocale
            model['locale'] = locale
            model['skill'] = skillsService.getSkill(skillId)
            model['test'] = data
            return "cms/academy/test/form"
        }
    }

    /*
        --------------------------------------
        Quiz - Course association
        --------------------------------------
     */

    @RequestMapping(path = "/cms/academy/{skillId}/quiz", method = RequestMethod.GET)
    String quizCourse(@PathVariable final Long skillId, final Model model) {
        model['skill'] = skillsService.getSkill(skillId)
        model['course'] = courseService.getCourse(skillId)
        model['quizzes'] = quizService.list()
        return "cms/academy/coursequiz"
    }

    @RequestMapping(path = "/cms/academy/{skillId}/quiz", method = RequestMethod.POST)
    String quizCourseSave(@PathVariable final Long skillId,
                          @RequestParam final Map<String, String> data,
                          final Model model,
                          final RedirectAttributes redirAttrs) {
        if (!data['quizId']) {
            model['status'] = 'error'
            return quizCourse(skillId, model)
        }

        Long quizId = Long.valueOf(data['quizId'])
        courseService.saveQuiz(skillId, quizId)
        redirAttrs.addFlashAttribute('status', 'success')
        return WebUtils.redirectTo("/cms/academy/{}/quiz", skillId)
    }

    /*
        --------------------------------------
        Quiz page content admin
        --------------------------------------
     */

    @RequestMapping(path = "/cms/academy/quiz", method = RequestMethod.GET)
    String quizList(final Model model) {
        model['quizzes'] = quizService.list()
        return "cms/academy/quiz/list"
    }

    @RequestMapping(path = "/cms/academy/quiz/new", method = RequestMethod.GET)
    String quizNew(final Model model) {
        model['max_questions'] = MAX_ALLOWED_QUESTIONS
        model['max_answers'] = MAX_ALLOWED_ANSWERS
        model['supported_locale'] = I18nCommons.supportedLocale
        model['locale'] = I18nCommons.defaultLocale
        return "cms/academy/quiz/form"
    }

    @RequestMapping(path = "/cms/academy/quiz/{quizId}", method = RequestMethod.GET)
    String quizShow(@PathVariable final Long quizId, final Model model) {
        model['quiz'] = quizService.get(quizId)
        model['max_questions'] = MAX_ALLOWED_QUESTIONS
        model['max_answers'] = MAX_ALLOWED_ANSWERS
        model['supported_locale'] = I18nCommons.supportedLocale
        model['locale'] = I18nCommons.defaultLocale
        return "cms/academy/quiz/form"
    }

    @RequestMapping(path = "/cms/academy/quiz/{quizId}/delete", method = RequestMethod.GET)
    String quizDelete(@PathVariable final Long quizId, final Model model) {
        quizService.delete(quizId)
        return WebUtils.redirectTo("/cms/academy/quiz")
    }

    @RequestMapping(path = "/cms/academy/quiz", method = RequestMethod.POST)
    String quizSave(@RequestParam final Map<String, String> data,
                    final Model model,
                    final RedirectAttributes redirAttrs) {
        def locale = LocaleUtils.toLocale(data['locale'])
        def quizId = data['quiz_id'] ? data['quiz_id'].toLong() : null

        try {
            def quiz = quizService.saveQuiz(quizId, locale, MAX_ALLOWED_QUESTIONS, MAX_ALLOWED_ANSWERS, data)
            redirAttrs.addFlashAttribute('status', 'success')
            return WebUtils.redirectTo("/cms/academy/quiz/{}", quiz.id)
        } catch (Exception exp) {
            log.warn('There was an exception while saving ', exp)
            // provide null values in case missing
            data['id'] = data['id'] ?: null
            data['name'] = data['name'] ?: null
            data['questions'] = null
            // build modes
            model['status'] = 'error'
            model['quiz'] = data
            model['max_questions'] = MAX_ALLOWED_QUESTIONS
            model['max_answers'] = MAX_ALLOWED_ANSWERS
            model['supported_locale'] = I18nCommons.supportedLocale
            model['locale'] = I18nCommons.defaultLocale
            return "cms/academy/quiz/form"
        }
    }

}
