package tbt.cms.controllers.academy

import groovy.util.logging.Slf4j
import org.apache.commons.lang.LocaleUtils
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import tbt.app.common.util.WebUtils
import tbt.app.services.tag.TagService
import tbt.app.services.academy.CourseService
import tbt.app.services.skills.SkillsService
import tbt.cms.commons.utils.I18nCommons

@Controller
@Slf4j
class AcademyLessonController {

    public static final Range MAX_ALLOWED_LECTION = (1..35)

    private final SkillsService skillsService

    private final CourseService courseService

    private final TagService tagService

    AcademyLessonController(SkillsService skillsService,
                            CourseService courseService,
                            TagService tagService) {
        this.skillsService = skillsService
        this.courseService = courseService
        this.tagService = tagService
    }

    @RequestMapping(path = "/cms/academy/{skillId}/lesson", method = RequestMethod.GET)
    String form(@PathVariable final Long skillId, final Model model) {
        model['skill'] = skillsService.getSkill(skillId)
        model['course'] = courseService.getCourse(skillId)
        model['tags'] = tagService.list()
        model['max_lessons'] = MAX_ALLOWED_LECTION
        model['supported_locale'] = I18nCommons.supportedLocale
        model['locale'] = I18nCommons.defaultLocale
        return "cms/academy/lesson/form"
    }

    @RequestMapping(path = "/cms/academy/{skillId}/lesson", method = RequestMethod.POST)
    String testSave(@PathVariable final Long skillId,
                    @RequestParam final Map<String, String> data,
                    final Model model,
                    final RedirectAttributes redirAttrs) {
        def locale = LocaleUtils.toLocale(data['locale'])

        try {
            courseService.saveLessons(skillId, locale, MAX_ALLOWED_LECTION, data)
            redirAttrs.addFlashAttribute('status', 'success')
            return WebUtils.redirectTo("/cms/academy/{}/lesson", skillId)
        } catch (Exception exp) {
            log.warn('There was an exception while saving ', exp)
            return form(skillId, model)
        }
    }

}
