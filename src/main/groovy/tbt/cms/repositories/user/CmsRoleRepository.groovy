package tbt.cms.repositories.user

import org.springframework.data.repository.CrudRepository
import tbt.cms.domains.user.CmsRole

interface CmsRoleRepository extends CrudRepository<CmsRole, Long> {
}
