package tbt.cms.repositories.user

import org.springframework.data.repository.CrudRepository
import tbt.cms.domains.user.CmsUser


interface CmsUserRepository extends CrudRepository<CmsUser, Long> {

    CmsUser findByUsername(String username)

}
