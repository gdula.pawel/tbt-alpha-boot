package tbt.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import org.springframework.social.connect.ConnectionFactoryLocator
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository
import org.springframework.social.connect.web.ProviderSignInController
import tbt.app.common.security.CustomProviderSignInController
import tbt.app.common.security.FacebookSignInAdapter
import tbt.app.domains.user.Role
import tbt.app.services.security.FacebookConnectionSignupService
import tbt.app.services.security.SecurityService

import javax.sql.DataSource

@Configuration
@EnableWebSecurity
class SecurityConfig {

    @Configuration
    static class AppSecurityConfig extends WebSecurityConfigurerAdapter {

        private final ConnectionFactoryLocator connectionFactoryLocator

        private final UsersConnectionRepository usersConnectionRepository

        private final FacebookConnectionSignupService facebookConnectionSignup

        private final SecurityService securityService

        AppSecurityConfig(
                ConnectionFactoryLocator connectionFactoryLocator,
                UsersConnectionRepository usersConnectionRepository,
                FacebookConnectionSignupService facebookConnectionSignup,
                SecurityService securityService) {
            this.connectionFactoryLocator = connectionFactoryLocator
            this.usersConnectionRepository = usersConnectionRepository
            this.facebookConnectionSignup = facebookConnectionSignup
            this.securityService = securityService
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/css/**", "/js/**", "/images/**", "/fonts/**").permitAll()
                    .antMatchers("/signin/**", "/signup/**").permitAll()
                    .antMatchers("/contact", "/faq", "/termsandconditions").permitAll()
                    .anyRequest()
                    .hasAuthority(Role.USER.asAuthority())

                    .and()
                    .formLogin()
                    .loginPage("/")
                    .loginProcessingUrl("/login")
                    .failureUrl("/?status=loginError")
                    .defaultSuccessUrl(securityService.targetUrl())
                    .permitAll()

                    .and()
                    .logout()
                    .logoutUrl('/logout')
                    .logoutSuccessUrl("/?status=logoutSuccess")
                    .deleteCookies("JSESSIONID")
                    .permitAll()

                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/")

                    .and()
                    .rememberMe().disable()
                    .csrf().disable()


        }

        @Bean
        ProviderSignInController providerSignInController() {
            ((InMemoryUsersConnectionRepository) usersConnectionRepository).setConnectionSignUp(facebookConnectionSignup)

            new CustomProviderSignInController(
                    connectionFactoryLocator,
                    usersConnectionRepository,
                    new FacebookSignInAdapter(securityService.targetUrl())
            )
        }
    }

    @Configuration
    @Order(1)
    static class CmsSecurityConfig extends WebSecurityConfigurerAdapter {

        public static final String DEFAULT_SUCCESS_URL = "/cms/challenge"

        final DataSource dataSource

        CmsSecurityConfig(DataSource dataSource) {
            this.dataSource = dataSource
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/cms/**")
                    .authorizeRequests()
                    .anyRequest()
                    .hasAuthority(Role.ADMIN.asAuthority())

                    .and()
                    .formLogin()
                    .loginPage("/cms/login")
                    .loginProcessingUrl("/cms/auth/login")
                    .failureUrl("/cms/login?status=loginError")
                    .defaultSuccessUrl(DEFAULT_SUCCESS_URL)
                    .permitAll()

                    .and()
                    .logout()
                    .logoutUrl('/cms/auth/logout')
                    .logoutSuccessUrl("/cms/login?status=logoutSuccess")
                    .deleteCookies("JSESSIONID")
                    .permitAll()

                    .and()
                    .exceptionHandling()
                    .accessDeniedPage("/cms/login")

                    .and()
                    .rememberMe().disable()
                    .csrf().disable()
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .jdbcAuthentication()
                    .dataSource(dataSource)
                    .usersByUsernameQuery("select username, password, enabled from cms_user where username=?")
                    .authoritiesByUsernameQuery("select username, role from cms_user_roles where username=?")
                    .passwordEncoder(passwordEncoder())

        }

        @Bean
        AuthenticationEntryPoint entryPoint() {
            return new LoginUrlAuthenticationEntryPoint("/cms/login")
        }

        @Bean
        PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder()
        }
    }
}
