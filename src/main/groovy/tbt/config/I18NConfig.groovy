package tbt.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import org.springframework.web.servlet.i18n.SessionLocaleResolver

@Configuration
class I18NConfig extends WebMvcConfigurerAdapter {

    @Bean
    LocaleResolver localeResolver() {
        final SessionLocaleResolver slr = new SessionLocaleResolver()
        slr.setDefaultLocale(Locale.FRANCE)
        return slr
    }

    @Bean
    LocaleChangeInterceptor localeChangeInterceptor() {
        final LocaleChangeInterceptor lci = new LocaleChangeInterceptor()
        lci.setParamName("lang")
        return lci
    }

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor())
    }

    @Bean
    ReloadableResourceBundleMessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource()
        messageSource.setDefaultEncoding("UTF-8")
        messageSource.setBasename("classpath:i18n/messages")
        messageSource.setCacheSeconds(1) //refresh cache once per second
        return messageSource
    }


}
