package tbt.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.Environment
import reactor.spring.context.config.EnableReactor

@EnableReactor
@Configuration
class ReactorConfig {

    @Bean
    Environment env() {
        return Environment.initializeIfEmpty().assignErrorJournal()
    }
    
}
