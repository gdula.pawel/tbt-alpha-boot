package tbt.config

import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import org.springframework.metrics.export.datadog.EnableDatadogMetrics
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import tbt.app.interceptors.Interceptor

@EnableDatadogMetrics
@EnableCaching
@EnableAsync
@Configuration
class AppConfig extends WebMvcConfigurerAdapter {

    private final List<Interceptor> interceptors

    AppConfig(List<Interceptor> interceptors) {
        this.interceptors = interceptors
    }

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        interceptors.each { registry.addInterceptor(it) }
    }

}
