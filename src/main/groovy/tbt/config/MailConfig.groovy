package tbt.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.JavaMailSenderImpl

@Configuration
class MailConfig {

    @Value('${mail.username}')
    private String username

    @Value('${mail.password}')
    private String password

    @Bean
    JavaMailSender getJavaMailSender() {
        final JavaMailSenderImpl mailSender = new JavaMailSenderImpl()
        mailSender.defaultEncoding = 'UTF-8'
        mailSender.setHost('smtp.sendgrid.net')
        mailSender.setPort(587)

        mailSender.setUsername(username)
        mailSender.setPassword(password)

        Properties props = mailSender.getJavaMailProperties()
        props.put('mail.transport.protocol', 'smtp')
        props.put('mail.smtp.auth', 'true')
        props.put('mail.smtp.starttls.enable', 'true')
        props.put('mail.debug', 'true')

        return mailSender
    }

}
