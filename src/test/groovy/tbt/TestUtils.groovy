package tbt

import com.google.gson.Gson
import tbt.app.domains.activity.Activity
import tbt.app.domains.activity.content.ActivityContent

class TestUtils {

    static def asActivity(List<ActivityContent> activities) {
        activities.withIndex().collect { ActivityContent body, Integer index ->
            new Activity(version: body.version(), type: body.type(), created: (new Date() + index), body: new Gson().toJson(body))
        }
    }

}