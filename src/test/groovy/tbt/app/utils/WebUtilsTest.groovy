package tbt.app.utils

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import tbt.app.common.util.WebUtils

import static org.assertj.core.api.Assertions.assertThat

@RunWith(SpringRunner.class)
@SpringBootTest
@JdbcTest
class WebUtilsTest {

    @Test
    void redirectTo() {
        assertThat(WebUtils.redirectTo('/test')).isEqualTo('redirect:/test')
    }

    @Test
    void redirectTo_withPlaceholders() {
        assertThat(WebUtils.redirectTo('/test/{}/a/{}/b', 1, 2, 3))
                .isEqualTo('redirect:/test/1/a/2/b')
    }

}