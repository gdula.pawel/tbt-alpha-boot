package tbt.app.services.score.impl

import spock.lang.Specification
import spock.lang.Unroll
import tbt.TestUtils
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ProfileRatingUpdatedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.score.PointsCalculator

class ProfileRatingUpdatePointsCalculatorTest extends Specification {

    PointsCalculator calculator

    ActivityRepository activityRepository

    def setup() {
        activityRepository = Mock()
        calculator = new ProfileRatingUpdatePointsCalculator(activityRepository)
    }

    @Unroll
    def "for #type calculator support execution is = #supported"() {
        expect:
        calculator.baseOn(type) == supported

        where:
        type << ActivityType.values()
        supported << ActivityType.values().collect { it == ActivityType.PROFILE_RATING_UPDATED }
    }

    @Unroll
    def "calculating score for: #body(#main) = #points"() {
        given:
        User user = new User(id: 1)
        activityRepository.findAllByUserIdAndType(1, ActivityType.PROFILE_RATING_UPDATED) >> { TestUtils.asActivity(body) }

        expect:
        calculator.calculate(user) == points

        where:
        body        | points
        $([])       | 0
        $([1])      | 10
        $([1, 1])   | 10
        $([1, 2])   | 20
        $([1], [1]) | 10
        $([1], [2]) | 20
    }

    private def $(final List<Long>... changes) {
        changes.collect { new ProfileRatingUpdatedV1(changes: it.collect { i -> new ProfileRatingUpdatedV1.Change(skillId: i) }) }
    }

}
