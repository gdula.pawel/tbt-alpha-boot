package tbt.app.services.score.impl

import org.junit.Ignore
import spock.lang.Specification
import spock.lang.Unroll
import tbt.TestUtils
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.AcademyQuizSubmittedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.score.PointsCalculator

class AcademyQuizPointsCalculatorTest extends Specification {

    PointsCalculator calculator

    ActivityRepository repository

    def setup() {
        repository = Mock()
        calculator = new AcademyQuizPointsCalculator(repository)
    }

    @Unroll
    def "for #type calculator support execution is = #supported"() {
        expect:
        calculator.baseOn(type) == supported

        where:
        type << ActivityType.values()
        supported << ActivityType.values().collect { it == ActivityType.ACADEMY_QUIZ_SUBMITTED }
    }

    @Unroll
    def "calculating score for: #body(#main) = #points"() {
        given:
        User user = new User(id: 1)
        repository.findAllByUserIdAndType(1, ActivityType.ACADEMY_QUIZ_SUBMITTED) >> { TestUtils.asActivity(attempts) }

        expect:
        calculator.calculate(user) == points

        where:
        attempts                                        | points
        $([])                                           | 0
        $([100])                                        | 200
        $([90, 100])                                    | 140
        $([80, 90, 100])                                | 104
        $([70, 80, 90, 100])                            | 69
        $([60, 70, 80, 90, 100])                        | 49
        $([50, 60, 70, 80, 90, 100])                    | 35
        $([40, 50, 60, 70, 80, 90, 100])                | 25
        $([30, 40, 50, 60, 70, 80, 90, 100])            | 18
        $([20, 30, 40, 50, 60, 70, 80, 90, 100])        | 12
        $([10, 20, 30, 40, 50, 60, 70, 80, 90, 100])    | 7
        $([0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]) | 3
        $([0, 100])                                     | 50
        $([0, 0, 100])                                  | 20
        $([0, 0, 0, 100])                               | 1
        $([0, 0, 0, 0, 100])                            | 1
        $([0, 0, 0, 0, 0, 100])                         | 1
        $([0, 100, 0])                                  | 34
    }

    private def $(final List<Float> changes) {
        changes.collect { new AcademyQuizSubmittedV1(courseId: 1, skillId: 1, score: it) }
    }


}
