package tbt.app.services.score.impl

import spock.lang.Specification
import spock.lang.Unroll
import tbt.TestUtils
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ChallengeVideoScoredV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.score.PointsCalculator

class AcademyTestVideoPointsCalculatorTest extends Specification {

    PointsCalculator calculator

    ActivityRepository repository

    def setup() {
        repository = Mock()
        calculator = new AcademyTestVideoPointsCalculator(repository)
    }

    @Unroll
    def "for #type calculator support execution is = #supported"() {
        expect:
        calculator.baseOn(type) == supported

        where:
        type << ActivityType.values()
        supported << ActivityType.values().collect { it == ActivityType.CHALLENGE_VIDEO_SCORED }
    }

    @Unroll
    def "calculating score for: #body(#main) = #points"() {
        given:
        User user = new User(id: 1)
        repository.findAllByUserIdAndType(1, ActivityType.CHALLENGE_VIDEO_SCORED) >> { TestUtils.asActivity(body) }

        expect:
        calculator.calculate(user) == points

        where:
        body                | points
        $()                 | 0
        $([10])             | 400
        $([5, 5])           | 400
        $([5, 10])          | 450
        $([5])              | 350
        $([5], [5])         | 700
        $([5, 10], [5, 10]) | 900
    }

    private def $(final List<Long>... lessonId) {
        lessonId.toList().withIndex().collect { List<Long> score, Integer index ->
            score.collect { new ChallengeVideoScoredV1(courseId: index, score: it) }
        }.flatten()
    }


}
