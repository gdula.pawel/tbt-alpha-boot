package tbt.app.services.score.impl

import spock.lang.Specification
import spock.lang.Unroll
import tbt.TestUtils
import tbt.app.domains.academy.Lesson
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.AcademyVideoSeenV1
import tbt.app.domains.user.User
import tbt.app.repositories.academy.LessonRepository
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.score.PointsCalculator

class AcademyWatchedVideoPointsCalculatorTest extends Specification {

    PointsCalculator calculator

    ActivityRepository activityRepository

    LessonRepository lessonRepository

    def setup() {
        activityRepository = Mock()
        lessonRepository = Mock()
        calculator = new AcademyWatchedVideoPointsCalculator(activityRepository, lessonRepository)
    }

    @Unroll
    def "for #type calculator support execution is = #supported"() {
        expect:
        calculator.baseOn(type) == supported

        where:
        type << ActivityType.values()
        supported << ActivityType.values().collect { it == ActivityType.ACADEMY_LESSON_VIDEO_SEEN }
    }

    @Unroll
    def "calculating score for: #body(#main) = #points"() {
        given:
        User user = new User(id: 1)
        activityRepository.findAllByUserIdAndType(1, ActivityType.ACADEMY_LESSON_VIDEO_SEEN) >> { TestUtils.asActivity(body) }
        lessonRepository.findOne(_) >> { args -> main[args[0]] }

        expect:
        calculator.calculate(user) == points

        where:
        body      | main                                                      | points
        $([])     | [:]                                                       | 0
        $([1])    | [1L: new Lesson(main: true)]                              | 15
        $([1, 1]) | [1L: new Lesson(main: true)]                              | 15
        $([1])    | [1L: new Lesson(main: false)]                             | 2
        $([1, 1]) | [1L: new Lesson(main: false)]                             | 2
        $([1, 2]) | [1L: new Lesson(main: false), 2L: new Lesson(main: true)] | 17
    }

    private def $(final List<Long> lessonId) {
        lessonId.collect { new AcademyVideoSeenV1(lessonId: it) }
    }

}
