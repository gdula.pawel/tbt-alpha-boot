package tbt.app.services.score.impl

import spock.lang.Specification
import spock.lang.Unroll
import tbt.TestUtils
import tbt.app.domains.activity.ActivityType
import tbt.app.domains.activity.content.ProfileInfoUpdatedV1
import tbt.app.domains.user.User
import tbt.app.repositories.activity.ActivityRepository
import tbt.app.services.score.PointsCalculator

class ProfileInfoUpdatePointsCalculatorTest extends Specification {

    PointsCalculator calculator

    ActivityRepository activityRepository

    def setup() {
        activityRepository = Mock()
        calculator = new ProfileInfoUpdatePointsCalculator(activityRepository)
    }

    @Unroll
    def "for #type calculator support execution is = #supported"() {
        expect:
        calculator.baseOn(type) == supported

        where:
        type << ActivityType.values()
        supported << ActivityType.values().collect { it == ActivityType.PROFILE_INFO_UPDATED }
    }

    @Unroll
    def "calculating score for: #body(#main) = #points"() {
        given:
        User user = new User(id: 1)
        activityRepository.findAllByUserIdAndType(1, ActivityType.PROFILE_INFO_UPDATED) >> { TestUtils.asActivity(body) }

        expect:
        calculator.calculate(user) == points

        where:
        body            | points
        $([])           | 0
        $(['a'])        | 20
        $(['a', 'a'])   | 20
        $(['a', 'b'])   | 40
        $(['a'], ['a']) | 20
        $(['a'], ['b']) | 40
    }

    private def $(final List<String>... changes) {
        changes.collect { new ProfileInfoUpdatedV1(changes: it.collect { i -> new ProfileInfoUpdatedV1.Change(name: i) }) }
    }
}
